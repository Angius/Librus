<?php
session_start();



if (!isset($_SESSION["studentID"]) /*|| !isset($_SESSION["studentEmail"]) */) {
    header('Location: ../index.php');
} else {

    $email = $_SESSION["studentEmail"];
    $studentID = $_SESSION['studentID'];
}

require('../includes/student_functions.php');
require ('../includes/db.php');
require ('../languages/en_EN.php');

$sql = "SELECT COUNT(*) FROM `message` WHERE `Read_Message` = 0 AND `Student_ID_Student` = :id";
$sth = $dbh->prepare($sql);
$sth->bindParam(':id', $_SESSION['studentID']);

try {
    $sth->execute();
} catch (PDOException $e) {
    exit($e->getMessage());
}

$unread = $sth->fetchColumn();

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../semantic/dist/semantic.js"></script>


</head>

<body>



<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <div class="ui right icon menu">

        <?php // Check unread messages
        if (isset($_SESSION['studentID']))
        {
            if ($unread == 0) {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular mail outline icon"></i>
                            ' . $locale_messages . '
                       </a>');
            } else {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular inverted red mail outline icon unread"></i>
                            ' . $locale_messages . '
                       </a>');
            }
        }
        ?>

        <a href="logout.php" class="item">
            <i class="ui circular sign out icon"></i>
            <?=$locale_logout?>
        </a>
    </div>
</div>

<div class="row"></div>
<div class="ui middle aligned three column centered grid">

    <div class="three wide column">
        <div class="ui secondary vertical pointing menu">
            <a href="grades.php" class="item">
                Grades
            </a>
            <a href="subjectsAndTechers.php" class="active item">
                Subjects and Teachers
            </a>
            <a href="attendance.php" class="item">
                Attendance
            </a>

            <?php
            if ($unread == 0)
            {
                echo (' <a href="messages.php" class="item">
                            '. $locale_messages .'
                        </a>');
            }
            else
            {
                echo (' <a href="messages.php" class="item">
                            '. $locale_messages .'                                    
                            <div class="ui red left pointing unread label">'.$unread.'</div>
                        </a>');
            }
            ?>

        </div>
    </div>

    <div class="eight wide center column">
        <div class="ui raised segment">
            <table class="ui selectable celled table">
                <thead>
                <tr>
                    <th class="">Subject</th>
                    <th class="">Teacher name</th>
                    <th class="">Teacher last name</th>
                    <th class="">email</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $subjects = getAllStudentsSubjects($studentID);

                while ($row = $subjects->fetch(PDO::FETCH_ASSOC)) {
                    $subject = $row['SUBJECT'];
                    $name = $row['TEACHER_NAME'];
                    $lastName = $row['TEACHER_SURNAME'];
                    $email = $row['TEACHER_EMAIL'];

                    ?>
                    <tr>
                        <td> <?= $subject?> </td>
                        <td> <?= $name?> </td>
                        <td> <?= $lastName?> </td>
                        <td> <?= $email?> </td>
                    </tr>

                    <?php
                }
                ?>

                </tbody>

            </table>
        </div>


    </div>

</div>



<script>
    $('.ui.accordion').accordion();

    (function(){
        $('.unread')
            .transition('jiggle')
        ;
        setTimeout(arguments.callee, 3000);
    })();
</script>

</body>


</html>
