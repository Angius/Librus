<?php
session_start();

unset($_SESSION['studentID']);
unset( $students['Email_Student']);
unset( $students['Login_Admin']);

session_destroy();

header('Location: index.php?msg=logout');