<?php
// grab db connection

require('../includes/db.php');

// grab recaptcha library
require_once "../includes/recaptchalib.php";

// captcha secret key
$secret = "6LdDFz4UAAAAANVM40MdeZChzaolOuLX0BRD47Qe";

// empty response
$response = null;


// check secret key
$reCaptcha = new ReCaptcha($secret);

// verify captcha response


$response = $reCaptcha->verifyResponse(
    $_SERVER["REMOTE_ADDR"],
    $_POST["g-recaptcha-response"]
);




if ($response != null && $response->success)
{
    // Create SQL query
    $sql = "SELECT * FROM `student` WHERE `Email_Student` = '" . $_POST['email'] . "'";

    // prepare it
    $sth = $dbh->prepare($sql);

    // and try to execute
    try {
        $sth->execute();
    } catch (PDOException $e) {
        exit($e->getMessage());
    }

    // grab admin from db
    $students = $sth->fetch(PDO::FETCH_ASSOC);



    if (password_verify($_POST['pass'], $students['Password_Student']))
    {


        session_start();
        $_SESSION['studentID'] = $students['ID_Student'];
        $_SESSION['studentEmail'] = $students['Email_Student'];
        $_SESSION['studentlogin' ] = $students['Login_Admin'];
       header('Location: grades.php');
    }
    else
    {


        header('Location: index.php?msg=failed');
    }

}
else
{
    // redirect if captcha incorrect
    echo "FU m8";
    header('Location: index.php?msg=captcha');

}

