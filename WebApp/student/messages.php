<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
    require ("../languages/en_EN.php");
    require ("../includes/db.php");
    require ("../includes/generateToken.php");
    session_start();

    // Check if admin
    if (!isset($_SESSION['studentID']))
    {
        header('Location: index.php');
    }
    else
    {
        // Handle token
        $token = getToken(rand(10, 20));
        $_SESSION['token'] = $token;
    };
    ?>

</head>

<body>

<?php
if (isset($_SESSION['studentID']))
{

    $sql = "SELECT COUNT(*) FROM `message` WHERE `Read_Message` = 0 AND `Student_ID_Student` = :id";
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':id', $_SESSION['studentID']);

    try {
        $sth->execute();
    } catch (PDOException $e) {
        exit($e->getMessage());
    }

    $unread = $sth->fetchColumn();
}

?>

<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <div class="ui right icon menu">

        <?php // Check unread messages
        if (isset($_SESSION['studentID']))
        {
            if ($unread == 0) {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular mail outline icon"></i>
                            ' . $locale_messages . '
                       </a>');
            } else {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular inverted red mail outline icon unread"></i>
                            ' . $locale_messages . '
                       </a>');
            }
        }
        ?>

        <a href="logout.php" class="item">
            <i class="ui circular sign out icon"></i>
            <?=$locale_logout?>
        </a>
    </div>
</div>

<div class="ui middle aligned three column centered grid">

    <div class="row"></div>

    <div class="three wide column">
        <div class="ui secondary vertical pointing menu">
            <a href="grades.php" class="item">
                Grades
            </a>
            <a href="subjectsAndTechers.php" class="item">
                Subjects and Teachers
            </a>
            <a href="attendance.php" class="item">
                Attendance
            </a>

            <?php
            if ($unread == 0)
            {
                echo (' <a href="messages.php" class="active item">
                            '. $locale_messages .'
                        </a>');
            }
            else
            {
                echo (' <a href="messages.php" class="active item">
                            '. $locale_messages .'                                    
                            <div class="ui red left pointing unread label">'.$unread.'</div>
                        </a>');
            }
            ?>

        </div>
    </div>

    <div class="eight wide center column">

        <div class="ui raised segment">

            <?php
            $sql = "SELECT `message`.*, `teacher`.`Name_Teacher`, `teacher`.`Surname_Teacher`
                        FROM `teacher`
                            JOIN `message` ON `message`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher`
                        WHERE `Student_ID_Student` = :id 
                        ORDER BY `message`.`Read_Message` ASC";

            $sth = $dbh->prepare($sql);
            $sth->bindParam(':id', $_SESSION['studentID']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                exit($e->getMessage());
            }

            $messages = $sth->fetchAll  ();


            // Display something when no messages
            if (empty($messages)) {
                ?>

                <div class="ui tall stacked segment">
                    <div class="ui grid">

                        <div class="one wide column">
                            <a class="ui left corner label">
                                <i class="green mail outline icon"></i>
                            </a>
                        </div>
                        <div class="eleven wide column">
                            <h2 class="ui header"><?= $locale_nomessages ?></h2>
                        </div>
                        <div class="four wide column"></div>
                    </div>

                </div>

                <?php
            }
            foreach ($messages as $message)
            {
                if (!$message['Read_Message']) {
                    $messagelabel = '<i class="red mail icon"></i>';
                    $messagelink = 'href="read.php?token='.$token.'&id='.$message['ID_Message'].'"';
                    $messagesegment = '';
                } else {
                    $messagelabel = '<i class="open envelope icon"></i>';
                    $messagelink = '';
                    $messagesegment = ' secondary ';
                }

                ?>
                <div class="ui tall stacked segment<?=$messagesegment?>">
                    <div class="ui grid">

                        <div class="one wide column">
                            <a <?=$messagelink?> class="ui left corner label">
                                <?=$messagelabel?>
                            </a>
                        </div>
                        <div class="eleven wide column">
                            <h2 class="ui header"><?=$message['Topic_Message']?></h2>
                        </div>
                        <div class="four wide column">
                            <div>
                                <?=$message['Name_Teacher'].' '.$message['Surname_Teacher']?>
                            </div>
                        </div>
                    </div>

                    <div class="ui divider"></div>

                    <?=$message['Body_Message']?>

                    <div class="ui divider"></div>

                    <div class="ui label">
                        <i class="calendar icon"></i> <?=$message['Date_Message']?>
                    </div>

                </div>

                <?php
            }
            ?>

        </div>
    </div>

    <div class="three wide column">
        <div class="ui raised segment">
            <form class="ui form" action="add.php?type=message&token=<?=$token?>" method="post">

                <div class="field">
                    <label><?=$locale_messagetopic?></label>
                    <input name="topic" type="text" />
                </div>

                <div class="field">
                    <label><?=$locale_messagebody?></label>
                    <textarea name="body"></textarea>
                </div>

                <div class="field">
                    <label><?=$locale_teacher?></label>
                    <select name="teacher" class="ui fluid search dropdown" id="search-select">
                        <option value=""><?=$locale_teacher?></option>
                        <?php
                        // Fetch all students
                        $sth = $dbh -> prepare('SELECT * FROM `teacher` ORDER BY `Surname_Teacher` DESC');
                        $sth -> execute();
                        $teachers = $sth -> fetchAll();

                        // Print all students
                        foreach ($teachers as $row)
                        {
                            echo ('<option value="' . $row['ID_Teacher'] . '">' . $row['Name_Teacher'] . ' ' . $row['Surname_Teacher'] . '</option>');
                        }
                        ?>
                    </select>
                </div>

                <input type="hidden" name="token" value="<?=$token?>" />

                <div class="field">
                    <button class="ui fluid button" type="submit" ><?=$locale_send?></button>
                </div>
            </form>
        </div>
    </div>

</div>

<script>
    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;
</script>

</body>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
    (function(){
        $('.unread')
            .transition('jiggle')
        ;
        setTimeout(arguments.callee, 3000);
    })();

    $('.ui.dropdown')
        .dropdown()
    ;

    $('#search-select')
        .dropdown()
    ;

    $('#multi-select')
        .dropdown()
    ;
</script>



</html>