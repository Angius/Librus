<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <link rel="stylesheet" href="../css/styles.css">


    <?php
    require("../languages/en_EN.php");
    require("../includes/db.php");
    session_start();

    $sql = "SELECT COUNT(*) FROM `message` WHERE `Read_Message` = 0 AND `Student_ID_Student` = :id";
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':id', $_SESSION['teacherID']);

    try {
        $sth->execute();
    } catch (PDOException $e) {
        exit($e->getMessage());
    }

    $unread = $sth->fetchColumn();
    ?>

</head>

<body>

<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <div class="ui right icon menu">

        <?php // Check unread messages
        if (isset($_SESSION['studentID']))
        {
            if ($unread == 0) {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular mail outline icon"></i>
                            ' . $locale_messages . '
                       </a>');
            } else {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular inverted red mail outline icon unread"></i>
                            ' . $locale_messages . '
                       </a>');
            }
        }
        ?>

        <a href="logout.php" class="item">
            <i class="ui circular sign out icon"></i>
            <?=$locale_logout?>
        </a>
    </div>
</div>

<div class="ui middle aligned three column centered grid">
    <div class="row">
        <div class="column"></div>

        <div class="four wide center column">
            <div class="ui raised segment">

                <?php
                if (!isset($_SESSION['studentID'])) {

                    if (!empty($_GET)) {
                        if ($_GET['msg'] == 'failed') {
                            echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    Wrong credentials!
                                </div>
                                <p>Check your login and password and try again</p>
                           </div>');
                        } else if ($_GET['msg'] == 'error') {
                            echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    An error occured!
                                </div>
                                <p>Try again, if the error persists – contact your webmaster.</p>
                           </div>');
                        } else if ($_GET['msg'] == 'captcha') {
                            echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    Wrong captcha!
                                </div>
                                <p>You must be a robot!</p>
                           </div>');
                        }
                        else if ($_GET['msg'] == 'logout')
                        {
                            echo('<div class="ui positive message">
                                <i class="close icon"></i>
                                <div class="header">
                                    Installation successful!
                                </div>
                                <p>You\'ve been logged out!</p>
                           </div>');
                        }

                    }

                    ?>

                    <form class="ui form" action="login.php" method="post" id="login-form">

                        <h4 class="ui dividing header"><?= $locale_loginFormTitle ?></h4>

                        <div class="field">
                            <input name="email" placeholder="<?= $locale_email ?>" type="text"/>
                        </div>

                        <div class="field">
                            <label><?= $locale_password ?></label>
                            <div class="ui action input">
                                <input name="pass" type="password" id="pass">
                                <button class="ui icon button" type="button" id="showhide"
                                        onclick="toggle_password('pass')">
                                    <i class="grey eye icon"></i>
                                </button>
                            </div>
                        </div>

                        <div class="field">
                            <label><?= $locale_captcha ?></label>
                            <div class="g-recaptcha"
                                 data-sitekey="6LdDFz4UAAAAAFhxj6JgCz1YvhNhLq4ZGYF89BE-"></div>
                        </div>

                        <div class="field">
                            <button class="ui fluid button" type="submit"><?= $locale_loginButton ?></button>
                        </div>

                    </form>

                    <?php
                } else {

                    header('Location: grades.php');
                    ?>

                <?php } ?>
            </div>
        </div>
        <div class="column"></div>

    </div>
</div>

</body>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>


</html>