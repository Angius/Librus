<?php
    session_start();
    require ("../includes/db.php");

    if ($_GET['token'] == $_POST['token']) {

        // MESSAGE
        if ($_GET['type'] == 'message')
        {
            if (empty($_POST['topic']) ||
                empty($_POST['body']) ||
                empty($_POST['teacher']))
            {
                header('Location: messages.php?msg=empty');
            }
            else
            {
                $date = date('Y-m-d');

                $sql = "INSERT INTO `message` ( `ID_Message`,
                                                `Teacher_ID_Teacher`,
                                                `Student_ID_Student`,
                                                `Topic_Message`,
                                                `Body_Message`,
                                                `Date_Message`,
                                                `Read_Message`)
                        VALUES (NULL, :teacher, :student, :topic, :body, :date, '0');";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':teacher', $_POST['teacher']);
                $sth->bindParam(':student', $_SESSION['studentID']);
                $sth->bindParam(':topic', $_POST['topic']);
                $sth->bindParam(':body', $_POST['body']);
                $sth->bindParam(':date', $date);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: messages.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                    var_dump($_SESSION);
                }

                header('Location: messages.php?msg=success');
            }
        }


        else
        {
            header('Location: index.php?msg=error');
        }

    }
    else
    {
        echo "INVALID TOKEN";
        header('Location: index.php?msg=error');
    }