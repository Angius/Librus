<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
    require ("../languages/en_EN.php");
    require ("../includes/db.php");
    require ("../includes/generateToken.php");
    session_start();

    // Check if admin
    if (!isset($_SESSION['teacherID']))
    {
        header('Location: index.php');
    }
    else
    {
        // Handle token
        $token = getToken(rand(10, 20));
        $_SESSION['token'] = $token;
    };
    ?>

</head>

<body>

<?php
if (isset($_SESSION['teacherID']))
{

    $sql = "SELECT COUNT(*) FROM `message` WHERE `Read_Message` = 0 AND `Teacher_ID_Teacher` = :id";
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':id', $_SESSION['teacherID']);

    try {
        $sth->execute();
    } catch (PDOException $e) {
        exit($e->getMessage());
    }

    $unread = $sth->fetchColumn();
}
?>

<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <a href="logout.php" class="ui right icon menu">

        <?php // Check unread messages
        if (isset($_SESSION['teacherID']))
        {
            if ($unread == 0) {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular mail outline icon"></i>
                            ' . $locale_messages . '
                       </a>');
            } else {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular inverted red mail outline icon unread"></i>
                            ' . $locale_messages . '
                       </a>');
            }
        }
        ?>

        <a href="logout.php" class="item">
            <i class="ui circular sign out icon"></i>
            <?=$locale_logout?>
        </a>
    </a>
</div>

<div class="ui middle aligned three column centered grid">

    <div class="row"></div>

    <div class="three wide column">
        <div class="ui secondary vertical pointing menu">
            <a href="index.php" class="item">
                <strong><?= $locale_home ?></strong>
            </a>
            <a href="presence.php" class="item">
                <?= $locale_presence ?>
            </a>
            <a href="grades.php" class="active item">
                <?= $locale_grades ?>
            </a>

            <?php
            if ($unread == 0)
            {
                echo (' <a href="messages.php" class="item">
                            '. $locale_messages .'
                        </a>');
            }
            else
            {
                echo (' <a href="messages.php" class="item">
                            '. $locale_messages .'                                    
                            <div class="ui red left pointing unread label">'.$unread.'</div>
                        </a>');
            }
            ?>

        </div>
    </div>

    <div class="eight wide center column">

        <?php // HANDLE MESSAGES
        if (!empty($_GET))
        {
            if ($_GET['msg'] == 'success')
            {
                echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                '.$locale_successheader.'
                            </div>
                            <p>'.$locale_successbody.'</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'empty')
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                '.$locale_emptyheader.'
                            </div>
                            <p>'.$locale_emptybody.'</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'err' && isset($_GET['err']))
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                '.$locale_errheader.'
                            </div>
                            <p><strong>'.$locale_errheader.' </strong><br>' . $_GET['err'] . '</p>
                       </div>');
            }
        }
        ?>


        <div class="ui raised segment">
            <div class="ui stacked segments">
                <?php
                $sql = "SELECT `gradesource`.*, `subject`.*, `teacher`.`ID_Teacher` 
                        FROM `gradesource` 
                            JOIN `subject` ON `gradesource`.`Subject_ID_Subject` = `subject`.`ID_Subject` 
                            JOIN `teacher` ON `gradesource`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher`
                        WHERE `ID_Teacher` = :id";

                $sth = $dbh->prepare($sql);
                $sth->bindParam(':id', $_SESSION['teacherID']);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    exit($e->getMessage());
                }

                $gradesources = $sth->fetchAll  ();

                // Display something when no gradesources
                if (empty($gradesources)) {
                    ?>

                    <div class="ui segment">
                        <div class="eleven wide column">
                            <h4 class="ui header"><?= $locale_nogradesources ?></h4>
                        </div>
                    </div>

                    <?php
                }
                foreach ($gradesources as $gradesource)
                {
                    ?>
                    <div class="ui clearing segment">

                        <div class="ui ribbon label">
                            <i class="calendar icon"></i> <?=$gradesource['Date_GradeSource']?>
                        </div>

                        <h4 class="ui right floated header"><?=$gradesource['Name_GradeSource']?></h4>

                        <a href="grade.php?gradesource=<?=$gradesource['ID_GradeSource']?>&name=<?=$gradesource['Name_GradeSource']?>" class="ui button">Grade</a>

                    </div>

                    <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="three wide column">
        <div class="ui raised segment">

            <form class="ui form" action="add.php?type=gradesource&token=<?=$token?>" method="post">

                <div class="field">
                    <label>Name</label>
                    <input name="name" type="text" />
                </div>

                <div class="field">
                    <label>Date</label>
                    <input name="date" type="date" />
                </div>

                <div class="field">
                    <label>Subjects</label>
                    <select name="subject" class="ui fluid search dropdown" id="search-select">
                        <option value="">Subject</option>
                        <?php
                        // Grab subjects taught in the class
                        $sql = "SELECT DISTINCT `subject`.*, `teacher`.`ID_Teacher` 
                                FROM `subject` 
                                    JOIN `class_has_subject` ON `class_has_subject`.`Subject_ID_Subject` = `subject`.`ID_Subject` 
                                    JOIN `teacher` ON `class_has_subject`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher` 
                                WHERE `teacher`.`ID_Teacher`=:id";

                        $sth = $dbh->prepare($sql);

                        $sth->bindParam(':id', $_SESSION['teacherID']);

                        try {
                            $sth->execute();
                        } catch (PDOException $e) {
                            exit($e->getMessage());
                        }

                        $subjects = $sth->fetchAll();

                        // Print all subjects
                        foreach ($subjects as $row)
                        {
                            echo ('<option value="' . $row['ID_Subject'] . '">' . $row['Name_Subject'] . '</option>');
                        }
                        ?>
                    </select>
                </div>

                <input type="hidden" name="token" value="<?=$token?>" />
                <input type="hidden" name="teacher" value="<?=$_SESSION['teacherID']?>" />

                <div class="field">
                    <button class="ui fluid button" type="submit" >Create</button>
                </div>
            </form>

        </div>
    </div>

</div>

<script>
    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;
</script>

</body>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
    (function(){
        $('.unread')
            .transition('jiggle')
        ;
        setTimeout(arguments.callee, 3000);
    })();

    $('#search-select')
        .dropdown()
    ;

    $('#search-select2')
        .dropdown()
    ;

</script>



</html>