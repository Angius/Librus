<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
    require ("../languages/en_EN.php");
    require ("../includes/db.php");
    session_start();
    ?>

</head>

<body>

    <?php
        if (isset($_SESSION['teacherID']))
        {

            $sql = "SELECT COUNT(*) FROM `message` WHERE `Read_Message` = 0 AND `Teacher_ID_Teacher` = :id";
            $sth = $dbh->prepare($sql);
            $sth->bindParam(':id', $_SESSION['teacherID']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                exit($e->getMessage());
            }

            $unread = $sth->fetchColumn();
        }
    ?>

    <div class="ui menu">
        <div class="header item">
            Librus
        </div>
        <a href="logout.php" class="ui right icon menu">

            <?php // Check unread messages
            if (isset($_SESSION['teacherID']))
            {
                if ($unread == 0) {
                    echo('<a href="messages.php" class="item">
                            <i class="ui circular mail outline icon"></i>
                            ' . $locale_messages . '
                       </a>');
                } else {
                    echo('<a href="messages.php" class="item">
                            <i class="ui circular inverted red mail outline icon unread"></i>
                            ' . $locale_messages . '
                       </a>');
                }
            }
            ?>

            <a href="logout.php" class="item">
                <i class="ui circular sign out icon"></i>
                <?=$locale_logout?>
            </a>
        </a>
    </div>

<div class="ui middle aligned three column centered grid">

    <?php
        if (!isset($_SESSION['teacherID'])) {
    ?>
        <div class="row"></div>

        <div class="six wide column"></div>

        <div class="four wide center column">

            <?php
                if (!empty($_GET))
                {
                    if ($_GET['msg'] == 'failed')
                    {
                        echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    '.$locale_wrongcredentials_head.'
                                </div>
                                <p>'.$locale_wrongcredentials_text.'</p>
                           </div>');
                    }
                    else if ($_GET['msg'] == 'error')
                    {
                        echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    '.$locale_error_head.'
                                </div>
                                <p>'.$locale_error_text.'</p>
                           </div>');
                    }
                    else if ($_GET['msg'] == 'captcha')
                    {
                        echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    '.$locale_wrongcaptcha_head.'
                                </div>
                                <p>'.$locale_wrongcaptcha_text.'</p>
                           </div>');
                    }
                    else if ($_GET['msg'] == 'logout')
                    {
                        echo('<div class="ui positive message">
                                <i class="close icon"></i>
                                <div class="header">
                                    '.$locale_logout_head.'
                                </div>
                                <p>'.$locale_logout_text.'</p>
                           </div>');
                    }
                }
            ?>

            <div class="ui raised segment">
                <form class="ui center form" action="login.php" method="post" id="login-form">

                    <h4 class="ui dividing header"><?= $locale_loginFormTitle ?></h4>

                    <div class="field">
                        <input name="login" placeholder="<?= $locale_login ?>" type="text"/>
                    </div>

                    <div class="field">
                        <label><?= $locale_password ?></label>
                        <div class="ui action input">
                            <input name="pass" type="password" id="pass">
                            <button class="ui icon button" type="button" id="showhide"
                                    onclick="toggle_password('pass')">
                                <i class="grey eye icon"></i>
                            </button>
                        </div>
                    </div>

                    <div class="field">
                        <label><?= $locale_captcha ?></label>
                        <div class="g-recaptcha"
                             data-sitekey="6LdDFz4UAAAAAFhxj6JgCz1YvhNhLq4ZGYF89BE-"></div>
                    </div>

                    <div class="field">
                        <button class="ui fluid button" type="submit"><?= $locale_loginButton ?></button>
                    </div>

                </form>
            </div>
        </div>

        <div class="six wide column"></div>

    <?php
        } else {
    ?>

            <div class="row"></div>

            <div class="three wide column">
                <div class="ui secondary vertical pointing menu">
                    <a class="active item">
                        <strong><?= $locale_home ?></strong>
                    </a>
                    <a href="presence.php" class="item">
                        <?= $locale_presence ?>
                    </a>
                    <a href="grades.php" class="item">
                        <?= $locale_grades ?>
                    </a>

                    <?php
                    if ($unread == 0)
                    {
                        echo (' <a href="messages.php" class="item">
                                    '. $locale_messages .'
                                </a>');
                    }
                    else
                    {
                        echo (' <a href="messages.php" class="item">
                                    '. $locale_messages .'                                    
                                    <div class="ui red left pointing unread label">'.$unread.'</div>
                                </a>');
                    }
                    ?>

                </div>
            </div>

            <div class="eight wide center column">

                <?php
                if (!empty($_GET))
                {
                    if ($_GET['msg'] == 'loggedin')
                    {
                        echo('<div class="ui positive message">
                                <i class="close icon"></i>
                                <div class="header">
                                    '.$locale_loggedin_head . $_SESSION['teachername'] . '</strong>
                                </div>
                                <p>'.$locale_loggedin_text.'</p>
                           </div>');
                    }
                }
                ?>

                <div class="ui raised segment">

                    <div class="ui cards">

                        <a href="presence.php" class="card">
                            <div class="content">
                                <div class="header"><?=$locale_presence ?></div>
                                <div class="description">
                                    <?=$locale_check ?>
                                </div>
                            </div>
                        </a>

                        <a href="grades.php" class="card">
                            <div class="content">
                                <div class="header"><?=$locale_grades ?></div>
                                <div class="description">
                                    <?=$locale_grade ?>
                                </div>
                            </div>
                        </a>

                        <a href="messages.php" class="card">
                            <div class="content">
                                <div class="header"><?=$locale_messages ?></div>
                                <div class="description">
                                    <?=$locale_seemessages ?>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>

            <div class="three wide column"></div>

        <?php
            }
        ?>
</div>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

</body>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
        (function(){
            $('.unread')
                .transition('jiggle')
            ;
            setTimeout(arguments.callee, 3000);
        })();

</script>



</html>