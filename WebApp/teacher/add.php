<?php
    session_start();
    require ("../includes/db.php");

    if ($_GET['token'] == $_POST['token']) {

        // MESSAGE
        if ($_GET['type'] == 'message')
        {
            if (empty($_POST['topic']) ||
                empty($_POST['body']) ||
                empty($_POST['students']))
            {
                header('Location: messages.php?msg=empty');
            }
            else
            {
                $date = date('Y-m-d');

                echo '<hr>';


                foreach ($_POST['students'] as $student)
                {
                    $sql = "INSERT INTO `message` ( `ID_Message`,
                                                    `Teacher_ID_Teacher`,
                                                    `Student_ID_Student`,
                                                    `Topic_Message`,
                                                    `Body_Message`,
                                                    `Date_Message`,
                                                    `Read_Message`)
                            VALUES (NULL, :teacher, :student, :topic, :body, :date, '0');";

                    $sth = $dbh->prepare($sql);

                    $sth->bindParam(':teacher', $_SESSION['teacherID']);
                    $sth->bindParam(':student', $student);
                    $sth->bindParam(':topic', $_POST['topic']);
                    $sth->bindParam(':body', $_POST['body']);
                    $sth->bindParam(':date', $date);

                    try {
                        $sth->execute();
                    } catch (PDOException $e) {
                        header('Location: messages.php?msg=err&err='.$e->getMessage());
                        exit($e->getMessage());
                        var_dump($_SESSION);
                    }
                }

                header('Location: messages.php?msg=success');
            }
        }

        // GRADESOURCE
        else if ($_GET['type'] == 'gradesource')
        {
            if (empty($_POST['name']) ||
                empty($_POST['date']) ||
                empty($_POST['subject']) ||
                empty($_POST['teacher']))
            {
                header('Location: grades.php?msg=empty');
            }
            else
            {
                $sql = "INSERT INTO `gradesource` ( `ID_GradeSource`,
                                                    `Name_GradeSource`,
                                                    `Date_GradeSource`,
                                                    `Subject_ID_Subject`,
                                                    `Teacher_ID_Teacher`) 
                               VALUES (NULL, :name, :date, :subject, :teacher);";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':name', $_POST['name']);
                $sth->bindParam(':date', $_POST['date']);
                $sth->bindParam(':subject', $_POST['subject']);
                $sth->bindParam(':teacher', $_POST['teacher']);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: grades.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                header('Location: grades.php?msg=success');
            }
        }

        // GRADES
        else if ($_GET['type'] == 'grades')
        {
            // Move only relevant data to array
            $num = count($_POST) - 3;
            $i = 0;

            $post = array();

            foreach ($_POST as $key => $val)
            {
                if(++$i > $num) break;
                $post[$key] = $val;
            }

            // Make an array of subject IDs

            $students = array_keys($post);
            $grades = array_values($post);

            var_dump($students);
            echo '<br>';
            var_dump($grades);
            echo '<hr>';
            var_dump($post);

            // Add teacher-subject pairs to database
            for ($j=0; $j<count($grades); $j++)
            {
                $sql = "INSERT INTO `student_has_gradesource` (`Student_ID_Student`, `GradeSource_ID_GradeSource`, `Grade`) 
                        VALUES (:student, :gradesource, :grade)";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':student', $students[$j]);
                $sth->bindParam(':gradesource', $_POST['gradesource']);
                $sth->bindParam(':grade', $grades[$j]);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: grade.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                echo count($grades);

                if ($j==count($grades)) break;
            }

            header('Location: grades.php?msg=success');

        }

        // ABSENCE
        else if ($_GET['type'] == 'absence')
        {
            if (empty($_POST['students']))
            {
                header('Location: presence.php?msg=empty');
            }
            else {

                $date = date('Y-m-d H:i:s');

                // Add absence
                $sql = "INSERT INTO `absence` (`ID_Absence`, `Date_Absence`) VALUES (NULL, :date)";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':date', $date);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: presence.php?msg=err&err=' . $e->getMessage());
                    exit($e->getMessage());
                }

                $last_id = $dbh->lastInsertId();

                // Pair absence with subject
                $sql = "INSERT INTO `absence_has_subject` (`absence_ID_Absence`, `subject_ID_Subject`) VALUES (:absence, :subject);";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':absence', $last_id);
                $sth->bindParam(':subject', $_POST['subject']);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: presence.php?msg=err&err=' . $e->getMessage());
                    exit($e->getMessage());
                }

                foreach ($_POST['students'] as $student) {
                    $sql = "INSERT INTO `student_has_absence` (`Absence_ID_Absence`, `Student_ID_Student`) VALUES (:absence, :student)";

                    $sth = $dbh->prepare($sql);

                    $sth->bindParam(':absence', $last_id);
                    $sth->bindParam(':student', $student);

                    try {
                        $sth->execute();
                    } catch (PDOException $e) {
                        header('Location: presence.php?msg=err&err=' . $e->getMessage());
                        exit($e->getMessage());
                    }
                }

                header('Location: presence.php?msg=success');
            }
        }

        else
        {
            header('Location: index.php?msg=error');
        }

    }
    else
    {
        echo "INVALID TOKEN";
        header('Location: index.php?msg=error');
    }