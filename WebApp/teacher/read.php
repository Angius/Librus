<?php
    session_start();

    require ("../includes/db.php");
    require ("../includes/generateToken.php");

    if ($_GET['token'] == $_SESSION['token']) {

        if (empty($_GET['id']))
        {
            //header('Location: index.php');
            var_dump($_GET);
        }
        else
        {
            $sql = "UPDATE `message` SET `Read_Message` = 1
                    WHERE `ID_Message` = :id;";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':id', $_GET['id']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: messages.php?msg=err&err=' . $e->getMessage());
                exit($e->getMessage());
            }

            header('Location: messages.php?msg=updated');
        }

    }
    else
    {
        echo "INVALID TOKEN<hr>GET: " . $_GET['token'] . "<br>SESSION: " . $_SESSION['token'] . "<br>POST: ". $_POST['token'];
        header('Location: index.php?msg=error');
    }