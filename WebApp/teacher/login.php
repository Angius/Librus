<?php
    //  start session
    session_start();

    // grab db connection
    require ('../includes/db.php');

    // grab recaptcha library
    require_once "../includes/recaptchalib.php";

    // captcha secret key
    $secret = "6LdDFz4UAAAAANVM40MdeZChzaolOuLX0BRD47Qe";

    // empty response
    $response = null;

    // check secret key
    $reCaptcha = new ReCaptcha($secret);

    // verify captcha response
    $response = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
    );

    if ($response != null && $response->success)
    {
        // Create SQL query
        $sql = "SELECT * FROM `teacher` WHERE `login_teacher` = :login";

        // prepare it
        $sth = $dbh->prepare($sql);

        $sth->bindParam(':login', $_POST['login']);

        // and try to execute
        try {
            $sth->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }

        // grab admin from db
        $teacher = $sth->fetch(PDO::FETCH_ASSOC);

        // verify password
        if (password_verify($_POST['pass'], $teacher['Password_Teacher']))
        {
            $_SESSION['teacherID'] = $teacher['ID_Teacher'];
            $_SESSION['teachername'] = $teacher['Name_Teacher'];
            header('Location: index.php?msg=loggedin');
        }
        else
        {
            header('Location: index.php?msg=failed');
        }

    }
    else
    {
        // redirect if captcha incorrect
        echo "FU m8";
        header('Location: index.php?msg=captcha');

    }
