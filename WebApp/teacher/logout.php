<?php
    session_start();

    unset($_SESSION['teacherID']);
    unset($_SESSION['teachername']);

    session_destroy();

    header('Location: index.php?msg=logout');