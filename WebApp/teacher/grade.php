<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
    require ("../languages/en_EN.php");
    require ("../includes/db.php");
    require ("../includes/generateToken.php");
    session_start();

    // Check if gradesource set
    if (!isset($_GET['gradesource']))
    {
        header('Location: grades.php');
    }


    // Check if admin
    if (!isset($_SESSION['teacherID']))
    {
        header('Location: index.php');
    }
    else
    {
        // Handle token
        $token = getToken(rand(10, 20));
        $_SESSION['token'] = $token;
    };
    ?>

</head>

<body>

<?php
if (isset($_SESSION['teacherID']))
{

    $sql = "SELECT COUNT(*) FROM `message` WHERE `Read_Message` = 0 AND `Teacher_ID_Teacher` = :id";
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':id', $_SESSION['teacherID']);

    try {
        $sth->execute();
    } catch (PDOException $e) {
        exit($e->getMessage());
    }

    $unread = $sth->fetchColumn();
}
?>

<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <a href="logout.php" class="ui right icon menu">

        <?php // Check unread messages
        if (isset($_SESSION['teacherID']))
        {
            if ($unread == 0) {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular mail outline icon"></i>
                            ' . $locale_messages . '
                       </a>');
            } else {
                echo('<a href="messages.php" class="item">
                            <i class="ui circular inverted red mail outline icon unread"></i>
                            ' . $locale_messages . '
                       </a>');
            }
        }
        ?>

        <a href="logout.php" class="item">
            <i class="ui circular sign out icon"></i>
            <?=$locale_logout?>
        </a>
    </a>
</div>

<div class="ui middle aligned three column centered grid">

    <div class="row"></div>

    <div class="three wide column">
        <div class="ui secondary vertical pointing menu">
            <a href="index.php" class="item">
                <strong><?= $locale_home ?></strong>
            </a>
            <a href="grades.php" class="red active item">
                <?= $locale_grading ?>
            </a>
            <a href="presence.php" class="item">
                <?= $locale_presence ?>
            </a>
            <a href="grades.php" class="item">
                <?= $locale_grades ?>
            </a>

            <?php
            if ($unread == 0)
            {
                echo (' <a href="messages.php" class="item">
                            '. $locale_messages .'
                        </a>');
            }
            else
            {
                echo (' <a href="messages.php" class="item">
                            '. $locale_messages .'                                    
                            <div class="ui red left pointing unread label">'.$unread.'</div>
                        </a>');
            }
            ?>

        </div>
    </div>

    <div class="eight wide center column">

        <?php // HANDLE MESSAGES
        if (!empty($_GET['msg']))
        {
            if ($_GET['msg'] == 'success')
            {
                echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                '.$locale_successheader.'
                            </div>
                            <p>'.$locale_successbody.'</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'empty')
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                '.$locale_emptyheader.'
                            </div>
                            <p>'.$locale_emptybody.'</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'err' && isset($_GET['err']))
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                '.$locale_errheader.'
                            </div>
                            <p><strong>'.$locale_errheader.' </strong><br>' . $_GET['err'] . '</p>
                       </div>');
            }
        }
        ?>


        <div class="ui raised segment">
            <h2 class="ui header"><?=$_GET['name']?></h2>
            <form class="ui form" action="add.php?type=grades&token=<?=$token?>" method="post">

                <?php
                // GET GRADESOURCES
                $sql = "SELECT * FROM `gradesource`
                        WHERE `ID_Gradesource` = :id;";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':id', $_GET['gradesource']);

                try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                $gradesource = $sth->fetch(PDO::FETCH_ASSOC);

                // GET STUDENTS
                $sql = "SELECT DISTINCT `student`.`ID_Student`, `student`.`Name_Student`, `student`.`Surname_Student`, `class`.`ID_Class`, `class_has_subject`.*
                        FROM `student`
                            JOIN `class` ON `student`.`Class_ID_Class` = `class`.`ID_Class`
                            JOIN `class_has_subject` ON `class_has_subject`.`Class_ID_Class` = `class`.`ID_Class`
                        WHERE `class_has_subject`.`Subject_ID_Subject` = :subject AND `class_has_subject`.`Teacher_ID_Teacher` = :teacher;";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':subject', $gradesource['Subject_ID_Subject']);
                $sth->bindParam(':teacher', $gradesource['Teacher_ID_Teacher']);

                try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                $students = $sth->fetchAll();

                ?>

                <table class="ui celled table">
                    <thead>
                    <tr><th><?=$locale_student?></th>
                        <th><?=$locale_grade?></th>
                    </tr></thead>
                    <tbody>

                    <?php
                    foreach ($students as $student)
                    {
                        echo (' <tr>
                                    <td>' . $student['Name_Student'] . ' ' . $student['Surname_Student'] . '</td>
                                    <td>
                                        <select name="' . $student['ID_Student'] . '" class="ui search dropdown">
                                            <option value="">'.$locale_grade.'</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="5">6</option>
                                        </select>
                                    </td>  
                                </tr>');
                    }
                    ?>
                    </tbody>
                </table>

                <input type="hidden" name="gradesource" value="<?=$_GET['gradesource']?>" />

                <input type="hidden" name="name" value="<?=$_GET['gradesource']?>" />

                <input type="hidden" name="token" value="<?=$token?>" />

                <div class="field">
                    <button class="ui fluid button" type="submit" ><?=$locale_gradebtn?></button>
                </div>
            </form>
        </div>
    </div>

    <div class="three wide column"></div>

</div>

<script>
    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;
</script>

</body>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
    (function(){
        $('.unread')
            .transition('jiggle')
        ;
        setTimeout(arguments.callee, 3000);
    })();

</script>

</html>

