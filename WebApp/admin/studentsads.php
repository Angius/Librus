<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
    require ("../languages/en_EN.php");
    require ("../includes/db.php");
    require ("../includes/generateToken.php");

    // Handle token
    $token = getToken(rand(10, 20));
    ?>

</head>

<body>

<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <a href="logout.php" class="right item">
        Logout
    </a>
</div>

<div class="ui middle aligned three column centered grid">

    <?php
    if (!empty($_GET))
    {
        if ($_GET['msg'] == 'installed')
        {
            echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                Installation successful!
                            </div>
                            <p><strong>Things to do:</strong></p>
                            <div class="ui bulleted list">
                                  <div class="item">Delete <i>//admin/setup.php</i></div>
                                  <div class="item">Log in with your admin credentials</div>
                             </div>
                       </div>');
        }
        else if ($_GET['msg'] == 'failed')
        {
            echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                Wrong credentials!
                            </div>
                            <p>Check your login and password and try again</p>
                       </div>');
        }
        else if ($_GET['msg'] == 'captcha')
        {
            echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                Wrong captcha!
                            </div>
                            <p>You must be a robot!</p>
                       </div>');
        }
        else if ($_GET['msg'] == 'logout')
        {
            echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                Installation successful!
                            </div>
                            <p>You\'ve been logged out!</p>
                       </div>');
        }
    }
    ?>


    <div class="row"></div>

    <div class="three wide column">
        <div class="ui secondary vertical pointing menu">
            <a href="index.php" class="item">
                Home
            </a>
            <a href="teachers.php" class="item">
                Teachers
            </a>
            <a href="students.php" class="item">
                Students
            </a>
            <a href="classes.php" class="active item">
                Classes
            </a>
            <a href="subjects.php" class="item">
                Subjects
            </a>
        </div>
    </div>

    <div class="eight wide center column">

        <?php
        // Grab subjects
        $sql = "SELECT `class`.*, `teacher`.`Name_Teacher`, `teacher`.`Surname_Teacher`
                FROM `teacher`
                    JOIN `class` ON `class`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher`";
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }

        $classes = $sth->fetchAll();
        ?>

        <div class="ui raised segment">

            <table class="ui selectable celled table">
                <thead>
                <tr>
                    <th class="sorted ascending">ID</th>
                    <th class="">Name</th>
                    <th class="">Year</th>
                    <th class="">Homeroom Teacher</th>
                    <th class="">Subjects</th>
                    <th class="">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($classes as $row) {

                    // Grab subjects taught in the class
                    $sql = "SELECT `class`.*, `subject`.*
                            FROM `subject`
                                JOIN `class_has_subject` ON `class_has_subject`.`Subject_ID_Subject` = `subject`.`ID_Subject`
                                JOIN `class` ON `class_has_subject`.`Class_ID_Class` = `class`.`ID_Class`
                             WHERE `class`.`ID_Class`=:class";

                    $sth = $dbh->prepare($sql);

                    $sth->bindParam(':class', $row['ID_Class']);

                    try {
                        $sth->execute();
                    } catch (PDOException $e) {
                        exit($e->getMessage());
                    }

                    $subjects = $sth->fetchAll();
                ?>
                    <tr>
                        <td><?=$row['ID_Class']?></td>
                        <td><?=$row['Name_Class']?></td>
                        <td><?=$row['Year_Class']?></td>
                        <td><?=$row['Surname_Teacher'].' '.$row['Name_Teacher']?></td>
                        <td>
                            <?php
                                foreach ($subjects as $sub)
                                {
                                    echo ('<div class="ui circular label">'.$sub['Name_Subject'].'<div class="detail">'.$sub['ID_Subject'].'</div></div>');
                                }
                            ?>
                        </td>
                        <td>
                            <div class="ui small icon buttons">
                                <a href="delete.php?class=<?=$row['ID_Class']?>" class="ui button" data-tooltip="Delete class">
                                    <i class="trash outline icon"></i>
                                </a>
                                <a href="edit.php?class=<?=$row['ID_Class']?>" class="ui button" data-tooltip="Edit class">
                                    <i class="edit icon"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

        </div>

    </div>

    <div class="four wide column">

        <div class="ui raised segment">

            <?php
            if (empty($_GET['class'])) {
                ?>

                <form class="ui form" action="add.php?type=class&token=<?=$token?>" method="post">

                    <div class="field">
                        <label>Name</label>
                        <input name="name" type="text" />
                    </div>

                    <div class="field">
                        <label>Year</label>
                        <input name="year" type="text" />
                    </div>

                    <?php
                    $sql = "SELECT * FROM teacher";
                    $sth = $dbh->prepare($sql);

                    try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                    $teachers = $sth->fetchAll();
                    ?>
                    <div class="field">
                        <label>Homeroom teacher</label>
                        <select name="teacher" class="ui fluid dropdown">
                            <option value="">Class</option>
                            <?php
                            foreach ($teachers as $teacher)
                            {
                                echo('<option value="' . $teacher['ID_Teacher'] . '">' . $teacher['Surname_Teacher'] . ' '. $teacher['Name_Teacher'] . '</option>');
                            }
                            ?>
                        </select>
                    </div>

                    <div class="field">
                        <label>Subjects</label>
                        <select name="subjects[]" class="ui fluid multiple search dropdown" id="multi-select">
                            <option value="">Subject</option>
                            <?php
                            // Fetch all subjects
                            $subjectsQ = $dbh -> prepare('SELECT * FROM `subject` ORDER BY `ID_subject` DESC');
                            $subjectsQ -> execute();
                            $subjects = $subjectsQ -> fetchAll();

                            // Print all subjects
                            foreach ($subjects as $row)
                            {
                                echo ('<option value="' . $row['ID_Subject'] . '">' . $row['Name_Subject'] . '</option>');
                            }
                            ?>
                        </select>
                    </div>

                    <input type="hidden" name="token" value="<?=$token?>" />

                    <div class="field">
                        <button class="ui fluid button" type="submit" >Create</button>
                    </div>
                </form>

                <?php
            } else {
                ?>
                <form class="ui form" action="add.php?type=subject-teacher&token=<?=$token?>" method="post">

                    <?php
                    // GET SUBJECTS
                    $sql = "SELECT `class_has_subject`.*, `class`.`Name_Class`, `class`.`ID_Class`, `subject`.`Name_Subject`, `subject`.`ID_Subject`
                                FROM `subject`
                                    JOIN `class_has_subject` ON `class_has_subject`.`Subject_ID_Subject` = `subject`.`ID_Subject`
                                    JOIN `class` ON `class_has_subject`.`Class_ID_Class` = `class`.`ID_Class`
                                WHERE `ID_Class` = :class";

                    $sth = $dbh->prepare($sql);

                    $sth->bindParam(':class', $_GET['class']);

                    $sth->execute();

                    $subjects = $sth->fetchAll();

                    // GET TEACHERS
                    $sql = "SELECT * FROM `teacher`";

                    $sth = $dbh->prepare($sql);

                    $sth->execute();

                    $teachers = $sth->fetchAll();
                    ?>

                    <table class="ui celled table">
                        <thead>
                        <tr><th>Subject</th>
                            <th>Teacher</th>
                        </tr></thead>
                        <tbody>

                        <?php
                        foreach ($subjects as $subject)
                        {
                            echo (' <tr>
                                            <td>' . $subject['Name_Subject'] . '</td>
                                            <td>
                                                <select name="' . $subject['ID_Subject'] . '" class="ui search dropdown">
                                                    <option value="">Teacher</option>');

                            foreach ($teachers as $teacher)
                            {
                                echo ('<option value="' . $teacher['ID_Teacher'] . '">' . $teacher['Name_Teacher'] . ' ' . $teacher['Surname_Teacher'] . '</option>');
                            }

                            echo('          </select>
                                            </td>  
                                        </tr>');
                        }
                        ?>
                        </tbody>
                    </table>

                    <input type="hidden" name="class" value="<?=$_GET['class']?>" />

                    <input type="hidden" name="token" value="<?=$token?>" />

                    <div class="field">
                        <button class="ui fluid button" type="submit" >Create</button>
                    </div>
                </form>
                <?php
            }
            ?>

        </div>

    </div>
</div>

<script>
    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;
</script>

</body>

<script src="../js/generatePassword.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/tablesort.js"></script>

<script>$('table').tablesort()</script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script>
    $('.ui.dropdown')
        .dropdown()
    ;

    $('#search-select')
        .dropdown()
    ;

    $('#multi-select')
        .dropdown()
    ;
</script>



</html>