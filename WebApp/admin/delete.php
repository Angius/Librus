<?php
require ("../includes/db.php");

session_start();

if ($_GET['token'] == $_SESSION['token']) {

    // STUDENT
    if (!empty($_GET['student']))
    {
        $sql = "DELETE FROM `student`
                WHERE `ID_Student`=:id";

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':id', $_GET['student']);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            header('Location: students.php?msg=err&err='.$e->getMessage());
            exit($e->getMessage());
        }

        header('Location: students.php?msg=deleted');
    }

    // TEACHER
    else if (!empty($_GET['teacher']))
    {
        // Grab everywhere the teacher is
        $sql = "SELECT `teacher`.`ID_Teacher`, `class`.`ID_Class`, `message`.`ID_Message`, `class_has_subject`.`Subject_ID_Subject`, `gradesource`.`ID_GradeSource`
                FROM `teacher`
                    LEFT JOIN `class` ON `class`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher`
                    LEFT JOIN `message` ON `message`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher`
                    LEFT JOIN `class_has_subject` ON `class_has_subject`.`Class_ID_Class` = `class`.`ID_Class`
                    LEFT JOIN `gradesource` ON `gradesource`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher`
                WHERE `ID_Teacher`=:teacher";
        $sth = $dbh->prepare($sql);

        $sth->bindParam(':teacher', $_GET['teacher']);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            header('Location: teachers.php?msg=err&err='.$e->getMessage());
            exit($e->getMessage());
        }

        $teachers = $sth->fetchAll();
        $getstring = '';

        $classes = '';
        $messages = '';
        $subjects = '';
        $gradesources = '';

        // Check if teacher is involved anywhere
        foreach ($teachers as $teacher)
        {
            if (!empty($teacher['ID_Class'])) $classes .= $teacher['ID_Class'] . '+';
            if (!empty($teacher['ID_Message'])) $messages .= $teacher['ID_Message'] . '+';
            if (!empty($teacher['Subject_ID_Subject'])) $subjects .= $teacher['Subject_ID_Subject'] . '+';
            if (!empty($teacher['ID_GradeSource'])) $gradesources .= $teacher['ID_GradeSource'] . '+';
        }

        if (!empty($classes)) $getstring .= 'class=' . substr($classes, 0, -1). '&';
        if (!empty($messages)) $getstring .= 'message=' . substr($messages, 0, -1). '&';
        if (!empty($subjects)) $getstring .= 'subject=' . substr($subjects, 0, -1). '&';
        if (!empty($gradesources)) $getstring .= 'gradesource=' . substr($gradesources, 0, -1). '&';

        if($getstring != '')
        {
            header('Location: teachers.php?msg=exists&' . substr($getstring, 0, -1));
        }
        else {

            $sql = "DELETE FROM `teacher`
                WHERE `ID_Teacher`=:id";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':id', $_GET['teacher']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: teachers.php?msg=err&err=' . $e->getMessage());
                exit($e->getMessage());
            }

            header('Location: teachers.php?msg=deleted');
        }

    }

    // SUBJECT
    else if (!empty($_GET['subject']))
    {
        // Grab everywhere the subject is
        $sql = "SELECT * FROM `class_has_subject`  
                WHERE `Subject_ID_Subject`=:subject";
        $sth = $dbh->prepare($sql);

        $sth->bindParam(':subject', $_GET['subject']);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            header('Location: subjects.php?msg=err&err='.$e->getMessage());
            exit($e->getMessage());
        }

        $subjects = $sth->fetchAll();

        $getstring = '';

        foreach ($subjects as $subject)
        {
            if (!empty($subject['Subject_ID_Subject'])) $getstring .= $subject['Class_ID_Class'] . '+';
        }

        if($getstring != '')
        {
            header('Location: subjects.php?msg=exists&classes=' . substr($getstring, 0, -1));
        }
        else {

            $sql = "DELETE FROM `subject`
                WHERE `ID_Subject`=:id";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':id', $_GET['subject']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: subjects.php?msg=err&err=' . $e->getMessage());
                exit($e->getMessage());
            }

            header('Location: subjects.php?msg=deleted');
        }

    }

    // CLASSES
    else if (!empty($_GET['class']))
    {

        // Grab everywhere the class is
        $sql = "SELECT `class`.`ID_Class`, `student`.`ID_Student`
                FROM `class`
                    JOIN `student` ON `student`.`Class_ID_Class` = `class`.`ID_Class` 
                WHERE `ID_Class`=:class";
        $sth = $dbh->prepare($sql);

        $sth->bindParam(':class', $_GET['class']);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            header('Location: classes.php?msg=err&err='.$e->getMessage());
            exit($e->getMessage());
        }

        $classes = $sth->fetchAll();

        $getstring = '';

        foreach ($classes as $class)
        {
            if (!empty($class['ID_Class'])) $getstring .= $class['ID_Student'] . '+';
        }

        if($getstring != '')
        {
            header('Location: classes.php?msg=exists&students=' . substr($getstring, 0, -1));
        }
        else
        {
            // Delete class-subject associations
            $sql = "DELETE FROM `class_has_subject`
                    WHERE `Class_ID_Class`=:id";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':id', $_GET['class']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: classes.php?msg=err&err=' . $e->getMessage());
                exit($e->getMessage());
            }

            // Delete class
            $sql = "DELETE FROM `class`
                    WHERE `ID_Class`=:id";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':id', $_GET['class']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: classes.php?msg=err&err=' . $e->getMessage());
                exit($e->getMessage());
            }

            header('Location: classes.php?msg=deleted');
        }


    }

    // ELSE
    else
    {
        header('Location: index.php?msg=error');
    }

}
else
{
    echo "INVALID TOKEN";

    header('Location: index.php?msg=error');
}