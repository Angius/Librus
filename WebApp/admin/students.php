<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
        session_start();
        require ("../languages/en_EN.php");
        require ("../includes/db.php");
        require ("../includes/generateToken.php");

        // Check if admin
        if (!isset($_SESSION['adminID']))
        {
            header('Location: index.php');
        }
        else
        {
            // Handle token
            $token = getToken(rand(10, 20));
            $_SESSION['token'] = $token;
        };
    ?>

</head>

<body>

<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <a href="logout.php" class="right item">
        Logout
    </a>
</div>

<div class="ui middle aligned three column centered grid">


    <div class="row"></div>

    <div class="three wide column">
        <div class="ui secondary vertical pointing menu">
            <a href="index.php" class="item">
                <strong>Home</strong>
            </a>
            <a href="teachers.php" class="item">
                Teachers
            </a>
            <a href="students.php" class="active item">
                Students
            </a>
            <a href="classes.php" class="item">
                Classes
            </a>
            <a href="subjects.php" class="item">
                Subjects
            </a>
        </div>
    </div>

    <div class="eight wide center column">

        <?php // HANDLE MESSAGES
        if (!empty($_GET))
        {
            if ($_GET['msg'] == 'success')
            {
                echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                Success!
                            </div>
                            <p>Student has been added to database!</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'deleted')
            {
                echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                Success!
                            </div>
                            <p>Student has been deleted from database!</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'empty')
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                Empty fields!
                            </div>
                            <p>Only apartment number can be left empty!</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'err' && isset($_GET['err']))
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                Database error!
                            </div>
                            <p><strong>Error code: </strong><br>' . $_GET['err'] . '</p>
                       </div>');
            }
        }
        ?>

        <?php // GRAB ALL STUDENTS FROM DB
            $sql = "SELECT `student`.*, `class`.*
                        FROM `class`
                            JOIN `student` ON `student`.`Class_ID_Class` = `class`.`ID_Class`";
            $sth = $dbh->prepare($sql);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                exit($e->getMessage());
            }

            $students = $sth->fetchAll();
        ?>

        <div class="ui raised segment">

            <table class="ui selectable celled table">
                <thead>
                    <tr>
                        <th class="sorted ascending">ID</th>
                        <th class="">Surname</th>
                        <th class="">Name</th>
                        <th class="">Email</th>
                        <th class="">PESEL</th>
                        <th class="">Birthday</th>
                        <th class="">Address</th>
                        <th class="">Class</th>
                        <th class="">Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach($students as $row) {

                    // Parse address number
                    if ($row['Apartment_Student'] == '') $number = $row['Building_Student'];
                        else $number = $row['Building_Student'].'/'.$row['Apartment_Student'];

                    // Parse zipcode
                    $zipcode = substr_replace($row['Zipcode_Student'], '-', 2, 0);

                    // Parse date
                    $date = strtotime($row['Birthday_Student']);
                    $birthday = date('d.m.Y', $date);
                ?>
                    <tr>
                        <td><?=$row['ID_Student']?></td>
                        <td><?=$row['Surname_Student']?></td>
                        <td><?=$row['Name_Student']?></td>
                        <td><?=$row['Email_Student']?></td>
                        <td><?=$row['PESEL_Student']?></td>
                        <td><?=$birthday?></td>
                        <td><?=$row['Street_Student'].' '.$number.'<br>'.$zipcode.' '.$row['City_Student']?></td>
                        <td><?=$row['Name_Class'].' ('.$row['Year_Class'].')'?></td>
                        <td>
                            <div class="ui small icon buttons">
                                <a href="delete.php?student=<?=$row['ID_Student']?>&token=<?=$token?>" class="ui button" data-tooltip="Delete student">
                                    <i class="trash outline icon"></i>
                                </a>
                                <a href="edit.php?student=<?=$row['ID_Student']?>&token=<?=$token?>" class="ui button" data-tooltip="Edit student">
                                    <i class="edit icon"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

        </div>

    </div>

    <div class="four wide column">

        <div class="ui raised segment">

            <form class="ui form" action="add.php?type=student&token=<?=$token?>" method="post" id="login-form">

                <div class="field">
                    <label>Name</label>
                    <div class="two fields">
                        <div class="field">
                            <input name="name" placeholder="Name" type="text" />
                        </div>
                        <div class="field">
                            <input name="surname" placeholder="Surname" type="text" />
                        </div>
                    </div>
                </div>

                <div class="field">
                    <label>Address</label>
                    <div class="fields">
                        <div class="ten wide field">
                            <input name="street" placeholder="Street" type="text" />
                        </div>
                        <div class="three wide field">
                            <input name="building_nr" placeholder="Nr" type="text" />
                        </div>
                        <div class="three wide field">
                            <input name="building_apt" placeholder="Apt" type="text" />
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div class="fields">
                        <div class="twelve wide field">
                            <input name="city" placeholder="City" type="text" />
                        </div>
                        <div class="four wide field">
                            <input name="zipcode" placeholder="Zipcode" type="text" />
                        </div>
                    </div>
                </div>

                <div class="field">
                    <label>Birthday</label>
                    <input name="birthday" type="date" />
                </div>

                <div class="field">
                    <label>PESEL</label>
                    <input name="pesel" type="text" />
                </div>

                <div class="field">
                    <label>Password</label>
                    <div class="ui action input">
                        <input name="pass" type="password" id="pass">
                        <button class="ui icon button" type="button" onclick='insertPassword(6, 2, 4, "pass")' >
                            <i class="random icon"></i>
                        </button>
                        <button class="ui icon button" type="button" id="showhide" onclick="toggle_password('pass')">
                            <i class="grey eye icon"></i>
                        </button>
                    </div>
                </div>

                <div class="field">
                    <label>Email</label>
                    <input name="email" placeholder="name@domain.com" type="email" />
                </div>

                <?php
                $sql = "SELECT * FROM class";
                $sth = $dbh->prepare($sql);

                try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                $classes = $sth->fetchAll();
                ?>
                <div class="field">
                    <label>Class</label>
                    <select name="class" class="ui fluid dropdown">
                        <option value="">Class</option>
                        <?php
                        foreach ($classes as $class)
                        {
                            echo('<option value="' . $class['ID_Class'] . '">' . $class['Name_Class'] . ': ' . $class['Year_Class'] . '</option>');
                        }
                        ?>
                    </select>
                </div>

                <input type="hidden" name="token" value="<?=$token?>" />

                <div class="field">
                    <button class="ui fluid button" type="submit" >Create</button>
                </div>
            </form>

        </div>

    </div>
</div>

<script>
    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;
</script>

</body>

<script src="../js/generatePassword.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/tablesort.js"></script>

<script>$('table').tablesort()</script>

<script src='https://www.google.com/recaptcha/api.js'></script>





</html>