<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <link rel="stylesheet" href="../css/styles.css">


    <?php
        require ("../includes/db.php");
        require ("../includes/generateToken.php");

        // Handle token
        $token = getToken(rand(10, 20));
    ?>

</head>


<body>

    <div class="ui menu">
        <div class="header item">
            Librus
        </div>
        <a class="right item">
            About
        </a>
    </div>

    <?php
        $sql = "-- phpMyAdmin SQL Dump
                -- version 4.7.7
                -- https://www.phpmyadmin.net/
                --
                -- Host: localhost:3306
                -- Generation Time: Feb 04, 2018 at 03:31 AM
                -- Server version: 5.7.18
                -- PHP Version: 7.1.5
                
                SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
                SET AUTOCOMMIT = 0;
                START TRANSACTION;
                SET time_zone = \"+00:00\";
                
                --
                -- Database: `librus`
                --
                CREATE DATABASE IF NOT EXISTS `librus` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
                USE `librus`;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `absence`
                --
                
                DROP TABLE IF EXISTS `absence`;
                CREATE TABLE `absence` (
                  `ID_Absence` int(11) NOT NULL,
                  `Date_Absence` datetime NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `absence_has_subject`
                --
                
                DROP TABLE IF EXISTS `absence_has_subject`;
                CREATE TABLE `absence_has_subject` (
                  `absence_ID_Absence` int(11) NOT NULL,
                  `subject_ID_Subject` int(11) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `admin`
                --
                
                DROP TABLE IF EXISTS `admin`;
                CREATE TABLE `admin` (
                  `ID_Admin` int(11) NOT NULL,
                  `Login_Admin` varchar(45) NOT NULL,
                  `Password_Admin` varchar(255) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `class`
                --
                
                DROP TABLE IF EXISTS `class`;
                CREATE TABLE `class` (
                  `ID_Class` int(11) NOT NULL,
                  `Name_Class` char(1) NOT NULL,
                  `Year_Class` year(4) NOT NULL,
                  `Teacher_ID_Teacher` int(11) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `class_has_subject`
                --
                
                DROP TABLE IF EXISTS `class_has_subject`;
                CREATE TABLE `class_has_subject` (
                  `Class_ID_Class` int(11) NOT NULL,
                  `Subject_ID_Subject` int(11) NOT NULL,
                  `Teacher_ID_Teacher` int(11) DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `gradesource`
                --
                
                DROP TABLE IF EXISTS `gradesource`;
                CREATE TABLE `gradesource` (
                  `ID_GradeSource` int(11) NOT NULL,
                  `Name_GradeSource` varchar(45) NOT NULL,
                  `Date_GradeSource` date NOT NULL,
                  `Subject_ID_Subject` int(11) NOT NULL,
                  `Teacher_ID_Teacher` int(11) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `message`
                --
                
                DROP TABLE IF EXISTS `message`;
                CREATE TABLE `message` (
                  `ID_Message` int(11) NOT NULL,
                  `Teacher_ID_Teacher` int(11) NOT NULL,
                  `Student_ID_Student` int(11) NOT NULL,
                  `Topic_Message` varchar(45) NOT NULL,
                  `Body_Message` varchar(511) NOT NULL,
                  `Date_Message` date NOT NULL,
                  `Read_Message` tinyint(1) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `student`
                --
                
                DROP TABLE IF EXISTS `student`;
                CREATE TABLE `student` (
                  `ID_Student` int(11) NOT NULL,
                  `Name_Student` varchar(45) NOT NULL,
                  `Surname_Student` varchar(45) NOT NULL,
                  `Password_Student` varchar(255) NOT NULL,
                  `Email_Student` varchar(45) NOT NULL,
                  `PESEL_Student` varchar(11) NOT NULL,
                  `Birthday_Student` date NOT NULL,
                  `Street_Student` varchar(45) NOT NULL,
                  `City_Student` varchar(45) NOT NULL,
                  `Building_Student` varchar(5) NOT NULL,
                  `Apartment_Student` varchar(5) DEFAULT NULL,
                  `Zipcode_Student` varchar(5) NOT NULL,
                  `Class_ID_Class` int(11) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `student_has_absence`
                --
                
                DROP TABLE IF EXISTS `student_has_absence`;
                CREATE TABLE `student_has_absence` (
                  `Absence_ID_Absence` int(11) NOT NULL,
                  `Student_ID_Student` int(11) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `student_has_gradesource`
                --
                
                DROP TABLE IF EXISTS `student_has_gradesource`;
                CREATE TABLE `student_has_gradesource` (
                  `Student_ID_Student` int(11) NOT NULL,
                  `GradeSource_ID_GradeSource` int(11) NOT NULL,
                  `Grade` int(11) DEFAULT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `subject`
                --
                
                DROP TABLE IF EXISTS `subject`;
                CREATE TABLE `subject` (
                  `ID_Subject` int(11) NOT NULL,
                  `Name_Subject` varchar(45) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                -- --------------------------------------------------------
                
                --
                -- Table structure for table `teacher`
                --
                
                DROP TABLE IF EXISTS `teacher`;
                CREATE TABLE `teacher` (
                  `ID_Teacher` int(11) NOT NULL,
                  `Name_Teacher` varchar(45) NOT NULL,
                  `Surname_Teacher` varchar(45) NOT NULL,
                  `PESEL_Teacher` varchar(11) NOT NULL,
                  `Password_Teacher` varchar(255) NOT NULL,
                  `Login_Teacher` varchar(45) NOT NULL,
                  `Email_Teacher` varchar(45) NOT NULL,
                  `Street_Teacher` varchar(45) NOT NULL,
                  `City_Teacher` varchar(45) NOT NULL,
                  `Building_Teacher` varchar(5) NOT NULL,
                  `Apartment_Teacher` varchar(5) DEFAULT NULL,
                  `Zipcode_Teacher` varchar(5) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                
                --
                -- Indexes for dumped tables
                --
                
                --
                -- Indexes for table `absence`
                --
                ALTER TABLE `absence`
                  ADD PRIMARY KEY (`ID_Absence`);
                
                --
                -- Indexes for table `absence_has_subject`
                --
                ALTER TABLE `absence_has_subject`
                  ADD PRIMARY KEY (`absence_ID_Absence`,`subject_ID_Subject`),
                  ADD KEY `fk_absence_has_subject_subject1_idx` (`subject_ID_Subject`),
                  ADD KEY `fk_absence_has_subject_absence1_idx` (`absence_ID_Absence`);
                
                --
                -- Indexes for table `admin`
                --
                ALTER TABLE `admin`
                  ADD PRIMARY KEY (`ID_Admin`);
                
                --
                -- Indexes for table `class`
                --
                ALTER TABLE `class`
                  ADD PRIMARY KEY (`ID_Class`,`Teacher_ID_Teacher`),
                  ADD KEY `fk_Class_Teacher1_idx` (`Teacher_ID_Teacher`);
                
                --
                -- Indexes for table `class_has_subject`
                --
                ALTER TABLE `class_has_subject`
                  ADD PRIMARY KEY (`Class_ID_Class`,`Subject_ID_Subject`),
                  ADD KEY `fk_Class_has_Subject_Subject1_idx` (`Subject_ID_Subject`),
                  ADD KEY `fk_Class_has_Subject_Class1_idx` (`Class_ID_Class`),
                  ADD KEY `fk_Class_has_Subject_Teacher1_idx` (`Teacher_ID_Teacher`);
                
                --
                -- Indexes for table `gradesource`
                --
                ALTER TABLE `gradesource`
                  ADD PRIMARY KEY (`ID_GradeSource`,`Subject_ID_Subject`,`Teacher_ID_Teacher`),
                  ADD KEY `fk_GradeSource_Subject1_idx` (`Subject_ID_Subject`),
                  ADD KEY `fk_GradeSource_Teacher1_idx` (`Teacher_ID_Teacher`);
                
                --
                -- Indexes for table `message`
                --
                ALTER TABLE `message`
                  ADD PRIMARY KEY (`ID_Message`,`Student_ID_Student`,`Teacher_ID_Teacher`),
                  ADD KEY `fk_Teacher_has_Student_Student1_idx` (`Student_ID_Student`),
                  ADD KEY `fk_Teacher_has_Student_Teacher1_idx` (`Teacher_ID_Teacher`);
                
                --
                -- Indexes for table `student`
                --
                ALTER TABLE `student`
                  ADD PRIMARY KEY (`ID_Student`,`Class_ID_Class`),
                  ADD KEY `fk_Student_Class1_idx` (`Class_ID_Class`);
                
                --
                -- Indexes for table `student_has_absence`
                --
                ALTER TABLE `student_has_absence`
                  ADD PRIMARY KEY (`Absence_ID_Absence`,`Student_ID_Student`),
                  ADD KEY `fk_Attendance_has_Student_Student1_idx` (`Student_ID_Student`),
                  ADD KEY `fk_Attendance_has_Student_Attendance1_idx` (`Absence_ID_Absence`);
                
                --
                -- Indexes for table `student_has_gradesource`
                --
                ALTER TABLE `student_has_gradesource`
                  ADD PRIMARY KEY (`Student_ID_Student`,`GradeSource_ID_GradeSource`),
                  ADD KEY `fk_Student_has_GradeSource_GradeSource1_idx` (`GradeSource_ID_GradeSource`),
                  ADD KEY `fk_Student_has_GradeSource_Student1_idx` (`Student_ID_Student`);
                
                --
                -- Indexes for table `subject`
                --
                ALTER TABLE `subject`
                  ADD PRIMARY KEY (`ID_Subject`);
                
                --
                -- Indexes for table `teacher`
                --
                ALTER TABLE `teacher`
                  ADD PRIMARY KEY (`ID_Teacher`);
                
                --
                -- AUTO_INCREMENT for dumped tables
                --
                
                --
                -- AUTO_INCREMENT for table `absence`
                --
                ALTER TABLE `absence`
                  MODIFY `ID_Absence` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
                
                --
                -- AUTO_INCREMENT for table `admin`
                --
                ALTER TABLE `admin`
                  MODIFY `ID_Admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
                
                --
                -- AUTO_INCREMENT for table `class`
                --
                ALTER TABLE `class`
                  MODIFY `ID_Class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
                
                --
                -- AUTO_INCREMENT for table `gradesource`
                --
                ALTER TABLE `gradesource`
                  MODIFY `ID_GradeSource` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
                
                --
                -- AUTO_INCREMENT for table `message`
                --
                ALTER TABLE `message`
                  MODIFY `ID_Message` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
                
                --
                -- AUTO_INCREMENT for table `student`
                --
                ALTER TABLE `student`
                  MODIFY `ID_Student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
                
                --
                -- AUTO_INCREMENT for table `subject`
                --
                ALTER TABLE `subject`
                  MODIFY `ID_Subject` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
                
                --
                -- AUTO_INCREMENT for table `teacher`
                --
                ALTER TABLE `teacher`
                  MODIFY `ID_Teacher` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
                
                --
                -- Constraints for dumped tables
                --
                
                --
                -- Constraints for table `absence_has_subject`
                --
                ALTER TABLE `absence_has_subject`
                  ADD CONSTRAINT `fk_absence_has_subject_absence1` FOREIGN KEY (`absence_ID_Absence`) REFERENCES `absence` (`ID_Absence`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  ADD CONSTRAINT `fk_absence_has_subject_subject1` FOREIGN KEY (`subject_ID_Subject`) REFERENCES `subject` (`ID_Subject`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                
                --
                -- Constraints for table `class`
                --
                ALTER TABLE `class`
                  ADD CONSTRAINT `fk_Class_Teacher1` FOREIGN KEY (`Teacher_ID_Teacher`) REFERENCES `teacher` (`ID_Teacher`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                
                --
                -- Constraints for table `class_has_subject`
                --
                ALTER TABLE `class_has_subject`
                  ADD CONSTRAINT `fk_Class_has_Subject_Class1` FOREIGN KEY (`Class_ID_Class`) REFERENCES `class` (`ID_Class`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  ADD CONSTRAINT `fk_Class_has_Subject_Subject1` FOREIGN KEY (`Subject_ID_Subject`) REFERENCES `subject` (`ID_Subject`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  ADD CONSTRAINT `fk_Class_has_Subject_Teacher1` FOREIGN KEY (`Teacher_ID_Teacher`) REFERENCES `teacher` (`ID_Teacher`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                
                --
                -- Constraints for table `gradesource`
                --
                ALTER TABLE `gradesource`
                  ADD CONSTRAINT `fk_GradeSource_Subject1` FOREIGN KEY (`Subject_ID_Subject`) REFERENCES `subject` (`ID_Subject`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  ADD CONSTRAINT `fk_GradeSource_Teacher1` FOREIGN KEY (`Teacher_ID_Teacher`) REFERENCES `teacher` (`ID_Teacher`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                
                --
                -- Constraints for table `message`
                --
                ALTER TABLE `message`
                  ADD CONSTRAINT `fk_Teacher_has_Student_Student1` FOREIGN KEY (`Student_ID_Student`) REFERENCES `student` (`ID_Student`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  ADD CONSTRAINT `fk_Teacher_has_Student_Teacher1` FOREIGN KEY (`Teacher_ID_Teacher`) REFERENCES `teacher` (`ID_Teacher`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                
                --
                -- Constraints for table `student`
                --
                ALTER TABLE `student`
                  ADD CONSTRAINT `fk_Student_Class1` FOREIGN KEY (`Class_ID_Class`) REFERENCES `class` (`ID_Class`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                
                --
                -- Constraints for table `student_has_absence`
                --
                ALTER TABLE `student_has_absence`
                  ADD CONSTRAINT `fk_Attendance_has_Student_Attendance1` FOREIGN KEY (`Absence_ID_Absence`) REFERENCES `absence` (`ID_Absence`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  ADD CONSTRAINT `fk_Attendance_has_Student_Student1` FOREIGN KEY (`Student_ID_Student`) REFERENCES `student` (`ID_Student`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                
                --
                -- Constraints for table `student_has_gradesource`
                --
                ALTER TABLE `student_has_gradesource`
                  ADD CONSTRAINT `fk_Student_has_GradeSource_GradeSource1` FOREIGN KEY (`GradeSource_ID_GradeSource`) REFERENCES `gradesource` (`ID_GradeSource`) ON DELETE NO ACTION ON UPDATE NO ACTION,
                  ADD CONSTRAINT `fk_Student_has_GradeSource_Student1` FOREIGN KEY (`Student_ID_Student`) REFERENCES `student` (`ID_Student`) ON DELETE NO ACTION ON UPDATE NO ACTION;
                COMMIT;
                ";

    $sth = $dbh->prepare($sql);

    try {
        $sth->execute();
    } catch (PDOException $e) {
        exit($e->getMessage());
    }
    ?>

    <div class="ui middle aligned five column centered grid">

        <div class="row"></div>

        <div class="row">
            <div class="column"></div>

            <div class="column">
                <div class="ui raised segment">

                    <?php
                        if (isset($_GET['token']) && isset($_POST['token']) && $_GET['token'] == $_POST['token'])
                        {
                            $pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);

                            $sql = 'INSERT INTO admin (
                                          login_admin,
                                          password_admin)
                                    VALUES (:login, :password)';

                            $sth = $dbh->prepare($sql);

                            $sth->bindParam(':login', $_POST['login']);
                            $sth->bindParam(':password', $pass);

                            try {
                                $sth->execute();
                            } catch (PDOException $e) {
                                exit($e->getMessage());
                            }

                            header('Location: index.php?msg=installed');
                        }
                        else
                        {
                    ?>

                    <form class="ui form" action="setup.php?token=<?=$token?>" method="post" id="login-form">

                        <div class="field">
                            <label>Admin login</label>
                            <div class="two fields">
                                <div class="field">
                                    <input name="login" placeholder="Login" type="text" />
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <label>Password</label>
                            <div class="ui action input">
                                <input name="pass" type="password" id="pass">
                                <button class="ui icon button" type="button" onclick='insertPassword(6, 2, 4, "pass")' >
                                    <i class="random icon"></i>
                                </button>
                                <button class="ui icon button" type="button" id="showhide" onclick="toggle_password('pass')">
                                    <i class="grey eye icon"></i>
                                </button>
                            </div>
                        </div>

                        <input type="hidden" name="token" value="<?=$token?>" />

                        <div class="field">
                            <button class="ui fluid button" type="submit" >Create</button>
                        </div>
                    </form>

                    <?php
                        }
                    ?>
                </div>
            </div>

            <div class="column"></div>
        </div>
    </div>

</body>

<script src="../js/generatePassword.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src="../semantic/dist/semantic.js"></script>

</html>