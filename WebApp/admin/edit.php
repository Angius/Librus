<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
    session_start();
    require ("../languages/en_EN.php");
    require ("../includes/db.php");
    require ("../includes/generateToken.php");

    // Check if admin
    if (!isset($_SESSION['adminID']))
    {
        header('Location: index.php');
    }
    else
    {
        $token = getToken(rand(10, 20));
    }
    ?>

</head>
<body>

<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <a href="logout.php" class="right item">
        Logout
    </a>
</div>

<div class="ui middle aligned three column centered grid">

    <div class="row"></div>

    <div class="three wide column">
        <div class="ui secondary vertical pointing menu">
            <a href="index.php" class="item">
                <strong>Home</strong>
            </a>
            <a href="" class="active red item">
                Editing
            </a>
            <a href="teachers.php" class="item">
                Teachers
            </a>
            <a href="students.php" class="item">
                Students
            </a>
            <a href="classes.php" class="item">
                Classes
            </a>
            <a href="subjects.php" class="item">
                Subjects
            </a>
        </div>
    </div>


<?php
if ($_GET['token'] == $_SESSION['token']) {

    // STUDENT
    if (!empty($_GET['student']))
    {
        $sql = "SELECT `student`.*, `class`.*
                FROM `class`
                    JOIN `student` ON `student`.`Class_ID_Class` = `class`.`ID_Class` 
                WHERE `ID_Student` = :id";

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':id', $_GET['student']);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }

        $student = $sth->fetch(PDO::FETCH_ASSOC);

        // Parse zipcode
        $zipcode = substr_replace($student['Zipcode_Student'], '-', 2, 0);

        ?>
            <div class="four wide column">
                <div class="ui raised segment">
                    <form class="ui form" action="save.php?type=student&token=<?=$token?>" method="post" id="login-form">

                        <div class="field">
                            <label>Name</label>
                            <div class="two fields">
                                <div class="field">
                                    <input name="name" placeholder="Name" type="text" value="<?=$student['Name_Student']?>" />
                                </div>
                                <div class="field">
                                    <input name="surname" placeholder="Surname" type="text" value="<?=$student['Surname_Student']?>" />
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <label>Address</label>
                            <div class="fields">
                                <div class="ten wide field">
                                    <input name="street" placeholder="Street" type="text" value="<?=$student['Street_Student']?>" />
                                </div>
                                <div class="three wide field">
                                    <input name="building_nr" placeholder="Nr" type="text" value="<?=$student['Building_Student']?>" />
                                </div>
                                <div class="three wide field">
                                    <input name="building_apt" placeholder="Apt" type="text" value="<?=$student['Apartment_Student']?>" />
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <div class="fields">
                                <div class="twelve wide field">
                                    <input name="city" placeholder="City" type="text" value="<?=$student['City_Student']?>" />
                                </div>
                                <div class="four wide field">
                                    <input name="zipcode" placeholder="Zipcode" type="text" value="<?=$zipcode?>" />
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <label>Birthday</label>
                            <input name="birthday" type="date" value="<?=$student['Birthday_Student']?>" />
                        </div>

                        <div class="field">
                            <label>PESEL</label>
                            <input name="pesel" type="text" value="<?=$student['PESEL_Student']?>" />
                        </div>

                        <div class="field">
                            <label>Password (leave empty to keep current one)</label>
                            <div class="ui action input">
                                <input name="pass" type="password" id="pass">
                                <button class="ui icon button" type="button" onclick='insertPassword(6, 2, 4, "pass")' >
                                    <i class="random icon"></i>
                                </button>
                                <button class="ui icon button" type="button" id="showhide" onclick="toggle_password('pass')">
                                    <i class="grey eye icon"></i>
                                </button>
                            </div>
                        </div>

                        <div class="field">
                            <label>Email</label>
                            <input name="email" placeholder="name@domain.com" type="email" value="<?=$student['Email_Student']?>" />
                        </div>

                        <?php
                        $sql = "SELECT * FROM class";
                        $sth = $dbh->prepare($sql);

                        try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                        $classes = $sth->fetchAll();
                        ?>
                        <div class="field">
                            <label>Class</label>
                            <select name="class" class="ui fluid dropdown">
                                <option value="">Class</option>
                                <?php
                                foreach ($classes as $class)
                                {
                                    ?>
                                        <option value="<?=$class['ID_Class']?>" <?php if($class['ID_Class']==$student['ID_Class']) echo 'selected="selected"'?>>
                                            <?=$class['Name_Class']?>: <?=$class['Year_Class']?>
                                        </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <input type="hidden" name="token" value="<?=$token?>" />
                        <input type="hidden" name="id" value="<?=$_GET['student']?>" />

                        <div class="field">
                            <button class="ui fluid button" type="submit" >Update</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="four wide column">
                <div class="ui raised segment">
                    <form class="ui form">

                        <div class="disabled field">
                            <label>Name</label>
                            <div class="two fields">
                                <div class="disabled field">
                                    <input name="name" placeholder="Name" type="text" value="<?=$student['Name_Student']?>" />
                                </div>
                                <div class="disabled field">
                                    <input name="surname" placeholder="Surname" type="text" value="<?=$student['Surname_Student']?>" />
                                </div>
                            </div>
                        </div>

                        <div class="disabled field">
                            <label>Address</label>
                            <div class="fields">
                                <div class="ten wide disabled field">
                                    <input name="street" placeholder="Street" type="text" value="<?=$student['Street_Student']?>" />
                                </div>
                                <div class="three wide disabled field">
                                    <input name="building_nr" placeholder="Nr" type="text" value="<?=$student['Building_Student']?>" />
                                </div>
                                <div class="three wide disabled field">
                                    <input name="building_apt" placeholder="Apt" type="text" value="<?=$student['Apartment_Student']?>" />
                                </div>
                            </div>
                        </div>

                        <div class="disabled field">
                            <div class="fields">
                                <div class="twelve wide disabled field">
                                    <input name="city" placeholder="City" type="text" value="<?=$student['City_Student']?>" />
                                </div>
                                <div class="four wide disabled field">
                                    <input name="zipcode" placeholder="Zipcode" type="text" value="<?=$zipcode?>" />
                                </div>
                            </div>
                        </div>

                        <div class="disabled field">
                            <label>Birthday</label>
                            <input name="birthday" type="date" value="<?=$student['Birthday_Student']?>" />
                        </div>

                        <div class="disabled field">
                            <label>PESEL</label>
                            <input name="pesel" type="text" value="<?=$student['PESEL_Student']?>" />
                        </div>

                        <div class="disabled field">
                            <label>Password</label>
                            <div class="ui action input">
                                <input name="pass" type="text" value="HIDDEN" id="pass">
                            </div>
                        </div>

                        <div class="disabled field">
                            <label>Email</label>
                            <input name="email" placeholder="name@domain.com" type="email" value="<?=$student['Email_Student']?>" />
                        </div>

                        <div class="disabled field">
                            <label>Class</label>
                            <select name="class" class="ui fluid dropdown">
                                <option>
                                    <?=$student['Name_Class']?>: <?=$student['Year_Class']?>
                                </option>
                            </select>
                        </div>

                        <div class="field">
                            <button class="ui disabled fluid button">Previous values</button>
                        </div>
                    </form>
                </div>
            </div>
        <?php
    }

    // TEACHER
    else if (!empty($_GET['teacher']))
    {
        $sql = "SELECT * FROM `teacher` WHERE `ID_Teacher` = :id";
        $sth = $dbh->prepare($sql);

        $sth->bindParam(':id', $_GET['teacher']);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }

        $teacher = $sth->fetch(PDO::FETCH_ASSOC);

        // Parse zipcode
        $zipcode = substr_replace($teacher['Zipcode_Teacher'], '-', 2, 0);

        ?>

        <div class="four wide column">
            <div class="ui raised segment">
                <form class="ui form" action="save.php?type=teacher&token=<?=$token?>" method="post">

                    <div class="field">
                        <label>Name</label>
                        <div class="two fields">
                            <div class="field">
                                <input name="name" placeholder="Name" type="text" value="<?=$teacher['Name_Teacher']?>" />
                            </div>
                            <div class="field">
                                <input name="surname" placeholder="Surname" type="text" value="<?=$teacher['Surname_Teacher']?>" />
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <label>Address</label>
                        <div class="fields">
                            <div class="ten wide field">
                                <input name="street" placeholder="Street" type="text" value="<?=$teacher['Street_Teacher']?>" />
                            </div>
                            <div class="three wide field">
                                <input name="building_nr" placeholder="Nr" type="text" value="<?=$teacher['Building_Teacher']?>" />
                            </div>
                            <div class="three wide field">
                                <input name="building_apt" placeholder="Apt" type="text" value="<?=$teacher['Apartment_Teacher']?>" />
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <div class="fields">
                            <div class="twelve wide field">
                                <input name="city" placeholder="City" type="text" value="<?=$teacher['City_Teacher']?>" />
                            </div>
                            <div class="four wide field">
                                <input name="zipcode" placeholder="Zipcode" type="text" value="<?=$teacher['Zipcode_Teacher']?>" />
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <label>PESEL</label>
                        <input name="pesel" type="text" value="<?=$teacher['PESEL_Teacher']?>" />
                    </div>

                    <div class="field">
                        <label>Login</label>
                        <input name="login" type="text" value="<?=$teacher['Login_Teacher']?>" />
                    </div>

                    <div class="field">
                        <label>Password (leave empty to keep current one)</label>
                        <div class="ui action input">
                            <input name="pass" type="password" id="pass">
                            <button class="ui icon button" type="button" onclick='insertPassword(6, 2, 4, "pass")' >
                                <i class="random icon"></i>
                            </button>
                            <button class="ui icon button" type="button" id="showhide" onclick="toggle_password('pass')">
                                <i class="grey eye icon"></i>
                            </button>
                        </div>
                    </div>

                    <div class="field">
                        <label>Email</label>
                        <input name="email" placeholder="name@domain.com" type="email" value="<?=$teacher['Email_Teacher']?>" />
                    </div>

                    <input type="hidden" name="token" value="<?=$token?>" />
                    <input type="hidden" name="id" value="<?=$_GET['teacher']?>" />

                    <div class="field">
                        <button class="ui fluid button" type="submit" >Update</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="four wide column">
            <div class="ui raised segment">
                <form class="ui form">

                    <div class="disabled field">
                        <label>Name</label>
                        <div class="two fields">
                            <div class="disabled field">
                                <input name="name" type="text" value="<?=$teacher['Name_Teacher']?>" />
                            </div>
                            <div class="disabled field">
                                <input name="surname" type="text" value="<?=$teacher['Surname_Teacher']?>" />
                            </div>
                        </div>
                    </div>

                    <div class="disabled field">
                        <label>Address</label>
                        <div class="fields">
                            <div class="ten wide disabled field">
                                <input name="street" type="text" value="<?=$teacher['Street_Teacher']?>" />
                            </div>
                            <div class="three wide disabled field">
                                <input name="building_nr" type="text" value="<?=$teacher['Building_Teacher']?>" />
                            </div>
                            <div class="three wide disabled field">
                                <input name="building_apt" type="text" value="<?=$teacher['Apartment_Teacher']?>" />
                            </div>
                        </div>
                    </div>

                    <div class="disabled field">
                        <div class="fields">
                            <div class="twelve wide disabled field">
                                <input name="city" type="text" value="<?=$teacher['City_Teacher']?>" />
                            </div>
                            <div class="four wide disabled field">
                                <input name="zipcode" type="text" value="<?=$zipcode?>" />
                            </div>
                        </div>
                    </div>

                    <div class="disabled field">
                        <label>PESEL</label>
                        <input name="pesel" type="text" value="<?=$teacher['PESEL_Teacher']?>" />
                    </div>

                    <div class="disabled field">
                        <label>Login</label>
                        <input name="login" type="text" value="<?=$teacher['Login_Teacher']?>" />
                    </div>

                    <div class="disabled field">
                        <label>Password</label>
                        <div class="ui action input">
                            <input name="pass" type="text" value="HIDDEN">
                        </div>
                    </div>

                    <div class="disabled field">
                        <label>Email</label>
                        <input name="email"type="email" value="<?=$teacher['Email_Teacher']?>" />
                    </div>

                    <div class="disabled field">
                        <button class="ui disabled fluid button" type="submit" >Previous values</button>
                    </div>
                </form>
            </div>
        </div>
        <?php
    }

    // SUBJECT
    else if (!empty($_GET['subject']))
    {
        $sql = "SELECT * FROM `subject` WHERE `ID_Subject` = :id";
        $sth = $dbh->prepare($sql);

        $sth->bindParam(':id', $_GET['subject']);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }

        $subject = $sth->fetch(PDO::FETCH_ASSOC);

        ?>

        <div class="four wide column">
            <div class="ui raised segment">
                <form class="ui form" action="save.php?type=subject&token=<?=$token?>" method="post">

                    <div class="field">
                        <label>Name</label>
                        <input name="name" type="text"  value="<?=$subject['Name_Subject']?>"/>
                    </div>

                    <input type="hidden" name="token" value="<?=$token?>" />
                    <input type="hidden" name="id" value="<?=$_GET['subject']?>" />

                    <div class="field">
                        <button class="ui fluid button" type="submit" >Update</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="four wide column">
            <div class="ui raised segment">
                <form class="ui form">

                    <div class="disabled field">
                        <label>Name</label>
                        <input name="name" type="text" value="<?=$subject['Name_Subject']?>"/>
                    </div>

                    <div class="field">
                        <button class="ui disabled fluid button" type="submit" >Previous values</button>
                    </div>
                </form>
            </div>
        </div>
        <?php
    }

    // CLASSES
    else if (!empty($_GET['class']))
    {
        // Grab subjects
        $sql = "SELECT `class`.*, `teacher`.`Name_Teacher`, `teacher`.`Surname_Teacher`
                FROM `teacher`
                    JOIN `class` ON `class`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher`
                WHERE `ID_Class` = :id";
        $sth = $dbh->prepare($sql);

        $sth->bindParam(':id', $_GET['class']);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }

        $class = $sth->fetch(PDO::FETCH_ASSOC);
        ?>

        <div class="four wide column">
            <div class="ui raised segment">
                <?php
                if (empty($_GET['classid'])) {
                    ?>

                    <form class="ui form" action="save.php?type=class&token=<?=$token?>" method="post">

                        <div class="field">
                            <label>Name</label>
                            <input name="name" type="text" value="<?=$class['Name_Class']?>"/>
                        </div>

                        <div class="field">
                            <label>Year</label>
                            <input name="year" type="text" value="<?=$class['Year_Class']?>"/>
                        </div>

                        <?php
                        $sql = "SELECT * FROM teacher";
                        $sth = $dbh->prepare($sql);

                        try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                        $teachers = $sth->fetchAll();
                        ?>
                        <div class="field">
                            <label>Homeroom teacher</label>
                            <select name="teacher" class="ui fluid dropdown">
                                <?php
                                foreach ($teachers as $teacher)
                                {
                                    ?>
                                    <option value="<?=$teacher['ID_Teacher']?>" <?php if($class['Teacher_ID_Teacher']==$teacher['ID_Teacher']) echo 'selected="selected"'?>>
                                        <?=$teacher['Surname_Teacher'] . ' '. $teacher['Name_Teacher']?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="field">
                            <label>Subjects</label>
                            <select name="subjects[]" class="ui fluid multiple search dropdown subjects" id="multi-select">
                                <option value="">Dupa</option>
                                <?php
                                // Fetch all subjects
                                $sth = $dbh -> prepare('SELECT * FROM `subject` ORDER BY `ID_subject` DESC');
                                $sth -> execute();
                                $subjects = $sth -> fetchAll();

                                // Add
                                foreach ($subjects as $row)
                                {
                                    echo ('<option value="' . $row['ID_Subject'] . '">' . $row['Name_Subject'] . '</option>');
                                }
                                ?>
                            </select>
                        </div>

                        <input type="hidden" name="token" value="<?=$token?>" />
                        <input type="hidden" name="id" value="<?=$_GET['class']?>" />

                        <div class="field">
                            <button class="ui fluid button" type="submit" >Update</button>
                        </div>
                    </form>

                    <?php
                } else {
                    ?>
                    <form class="ui form" action="add.php?type=subject-teacher&token=<?=$token?>" method="post">

                        <?php
                        // GET CLASSES
                        $sql = "SELECT `class_has_subject`.*, `class`.`Name_Class`, `class`.`ID_Class`, `subject`.`Name_Subject`, `subject`.`ID_Subject`
                                FROM `subject`
                                    JOIN `class_has_subject` ON `class_has_subject`.`Subject_ID_Subject` = `subject`.`ID_Subject`
                                    JOIN `class` ON `class_has_subject`.`Class_ID_Class` = `class`.`ID_Class`
                                WHERE `ID_Class` = :class";

                        $sth = $dbh->prepare($sql);

                        $sth->bindParam(':class', $_GET['classid']);

                        try {
                            $sth->execute();
                        } catch (PDOException $e) {
                            exit($e->getMessage());
                        }

                        $subjects = $sth->fetchAll();

                        // GET TEACHERS
                        $sql = "SELECT *
                                FROM `teacher`
                                ";

                        $sth = $dbh->prepare($sql);

                        $sth->bindParam(':class', $_GET['classid']);

                        try {
                            $sth->execute();
                        } catch (PDOException $e) {
                            exit($e->getMessage());
                        }

                        $teachers = $sth->fetchAll();
                        ?>

                        <table class="ui celled table">
                            <thead>
                            <tr><th>Subject</th>
                                <th>Teacher</th>
                            </tr></thead>
                            <tbody>

                            <?php
                            foreach ($subjects as $subject)
                            {
                                echo (' <tr>
                                            <td>' . $subject['Name_Subject'] . '</td>
                                            <td>
                                                <select name="' . $subject['ID_Subject'] . '" class="ui search dropdown">
                                                    <option value="">Teacher</option>');

                                foreach ($teachers as $teacher)
                                {
                                    ?>
                                    <option value="<?=$teacher['ID_Teacher']?>" <?php if($teacher['ID_Teacher']==$subject['Teacher_ID_Teacher']) echo 'selected="selected"'?>>
                                        <?=$teacher['Surname_Teacher'] . ' ' . $teacher['Name_Teacher']?>
                                    </option>
                                    <?php
                                }

                                echo('          </select>
                                            </td>  
                                        </tr>');
                            }
                            ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="class" value="<?=$_GET['class']?>" />

                        <input type="hidden" name="token" value="<?=$token?>" />

                        <div class="field">
                            <button class="ui fluid button" type="submit" >Reassign</button>
                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>
        </div>

        <div class="four wide column">
            <div class="ui raised segment">
                <?php
                if (empty($_GET['classid'])) {
                    ?>

                    <form class="ui form">

                        <div class="disabled field">
                            <label>Name</label>
                            <input name="name" type="text" value="<?=$class['Name_Class']?>"/>
                        </div>

                        <div class="disabled field">
                            <label>Year</label>
                            <input name="year" type="text" value="<?=$class['Year_Class']?>"/>
                        </div>

                        <?php
                        $sql = "SELECT * FROM teacher";
                        $sth = $dbh->prepare($sql);

                        try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                        $teachers = $sth->fetchAll();
                        ?>
                        <div class="disabled field">
                            <label>Homeroom teacher</label>
                            <select name="teacher" class="ui fluid dropdown">
                                <?php
                                foreach ($teachers as $teacher)
                                {
                                    ?>
                                    <option value="<?=$teacher['ID_Teacher']?>" <?php if($class['Teacher_ID_Teacher']==$teacher['ID_Teacher']) echo 'selected="selected"'?>>
                                        <?=$teacher['Surname_Teacher'] . ' '. $teacher['Name_Teacher']?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="disabled field">
                            <label>Subjects</label>
                            <select name="subjects[]" class="ui fluid multiple search dropdown subjects" id="multi-select">
                                <option value="">Dupa</option>
                                <?php
                                // Fetch all subjects
                                $sth = $dbh -> prepare('SELECT * FROM `subject` ORDER BY `ID_subject` DESC');
                                $sth -> execute();
                                $subjects = $sth -> fetchAll();

                                // Add
                                foreach ($subjects as $row)
                                {
                                    echo ('<option value="' . $row['ID_Subject'] . '">' . $row['Name_Subject'] . '</option>');
                                }
                                ?>
                            </select>
                        </div>

                        <div class="disabled field">
                            <button class="ui fluid button" >Previous values</button>
                        </div>
                    </form>

                    <?php
                } else {
                    ?>
                    <form class="ui form" action="add.php?type=subject-teacher&token=<?=$token?>" method="post">

                        <?php
                        // GET CLASSES
                        $sql = "SELECT `class_has_subject`.*, `class`.`Name_Class`, `class`.`ID_Class`, `subject`.`Name_Subject`, `subject`.`ID_Subject`
                                FROM `subject`
                                    JOIN `class_has_subject` ON `class_has_subject`.`Subject_ID_Subject` = `subject`.`ID_Subject`
                                    JOIN `class` ON `class_has_subject`.`Class_ID_Class` = `class`.`ID_Class`
                                WHERE `ID_Class` = :class";

                        $sth = $dbh->prepare($sql);

                        $sth->bindParam(':class', $_GET['classid']);

                        try {
                            $sth->execute();
                        } catch (PDOException $e) {
                            exit($e->getMessage());
                        }

                        $subjects = $sth->fetchAll();

                        // GET TEACHERS
                        $sql = "SELECT *
                                FROM `teacher`
                                ";

                        $sth = $dbh->prepare($sql);

                        $sth->bindParam(':class', $_GET['classid']);

                        try {
                            $sth->execute();
                        } catch (PDOException $e) {
                            exit($e->getMessage());
                        }

                        $teachers = $sth->fetchAll();
                        ?>

                        <table class="ui celled table">
                            <thead>
                            <tr><th>Subject</th>
                                <th>Teacher</th>
                            </tr></thead>
                            <tbody>

                            <?php
                            foreach ($subjects as $subject)
                            {
                                echo (' <tr>
                                            <td class="disabled">' . $subject['Name_Subject'] . '</td>
                                            <td>
                                                <select name="' . $subject['ID_Subject'] . '" class="ui disabled search dropdown">
                                                    <option value="">Teacher</option>');

                                foreach ($teachers as $teacher)
                                {
                                    ?>
                                    <option value="<?=$teacher['ID_Teacher']?>" <?php if($teacher['ID_Teacher']==$subject['Teacher_ID_Teacher']) echo 'selected="selected"'?>>
                                        <?=$teacher['Surname_Teacher'] . ' ' . $teacher['Name_Teacher']?>
                                    </option>
                                    <?php
                                }

                                echo('          </select>
                                            </td>  
                                        </tr>');
                            }
                            ?>
                            </tbody>
                        </table>

                        <div class="field">
                            <button class="ui disabled fluid button">Previous values</button>
                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>
        </div>
        <?php

        // Grab subjects to be moved to js function
        $sql = "SELECT `Subject_ID_Subject` FROM `class_has_subject` WHERE `Class_ID_Class` = :id";

        $sth = $dbh->prepare($sql);

        $sth->bindParam(':id', $_GET['class']);

        try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

        $subjects = $sth->fetchAll();

        $arr = array();

        foreach ($subjects as $subject)
        {
            array_push($arr, $subject['Subject_ID_Subject']);
        }
    }

    // ELSE
    else
    {
        header('Location: index.php?msg=error1');
    }

}
else
{
    echo "INVALID TOKEN".$_GET['token'] .'-'.$_SESSION['token'];

    //header('Location: index.php?msg=error');

}

?>

<script>
    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;
</script>

</body>

<script src="../js/generatePassword.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/tablesort.js"></script>

<script>$('table').tablesort()</script>

<script src='https://www.google.com/recaptcha/api.js'></script>


<script>

    $('.ui.dropdown')
        .dropdown()
    ;

    $('#search-select')
        .dropdown()
    ;

    $('#multi-select')
        .dropdown()
    ;

    $('.subjects')
        .dropdown('set selected',<?php echo json_encode($arr); ?>)
    ;
</script>

</html>