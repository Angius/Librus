<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
    require ("../languages/en_EN.php");
    require ("../includes/db.php");
    session_start();
    ?>

</head>

<body>

    <div class="ui menu">
        <div class="header item">
            Librus
        </div>
        <a href="logout.php" class="right item">
            Logout
        </a>
    </div>

<div class="ui middle aligned three column centered grid">

    <?php
        if (!isset($_SESSION['adminID'])) {
    ?>
        <div class="row"></div>

        <div class="six wide column"></div>

        <div class="four wide center column">

            <?php
                if (!empty($_GET))
                {
                    if ($_GET['msg'] == 'installed')
                    {
                        echo('<div class="ui positive message">
                                <i class="close icon"></i>
                                <div class="header">
                                    Installation successful!
                                </div>
                                <p><strong>Things to do:</strong></p>
                                <div class="ui bulleted list">
                                      <div class="item">Delete <i>//admin/setup.php</i></div>
                                      <div class="item">Log in with your admin credentials</div>
                                 </div>
                           </div>');
                    }
                    else if ($_GET['msg'] == 'failed')
                    {
                        echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    Wrong credentials!
                                </div>
                                <p>Check your login and password and try again</p>
                           </div>');
                    }
                    else if ($_GET['msg'] == 'error')
                    {
                        echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    An error occured!
                                </div>
                                <p>Try again, if the error persists – contact your webmaster.</p>
                           </div>');
                    }
                    else if ($_GET['msg'] == 'captcha')
                    {
                        echo('<div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    Wrong captcha!
                                </div>
                                <p>You must be a robot!</p>
                           </div>');
                    }
                    else if ($_GET['msg'] == 'logout')
                    {
                        echo('<div class="ui positive message">
                                <i class="close icon"></i>
                                <div class="header">
                                    See you later!
                                </div>
                                <p>You\'ve been logged out!</p>
                           </div>');
                    }
                }
            ?>

            <div class="ui raised segment">
                <form class="ui center form" action="login.php" method="post" id="login-form">

                    <h4 class="ui dividing header"><?= $locale_loginFormTitle ?></h4>

                    <div class="field">
                        <input name="login" placeholder="<?= $locale_login ?>" type="text"/>
                    </div>

                    <div class="field">
                        <label><?= $locale_password ?></label>
                        <div class="ui action input">
                            <input name="pass" type="password" id="pass">
                            <button class="ui icon button" type="button" id="showhide"
                                    onclick="toggle_password('pass')">
                                <i class="grey eye icon"></i>
                            </button>
                        </div>
                    </div>

                    <div class="field">
                        <label><?= $locale_captcha ?></label>
                        <div class="g-recaptcha"
                             data-sitekey="6LdDFz4UAAAAAFhxj6JgCz1YvhNhLq4ZGYF89BE-"></div>
                    </div>

                    <div class="field">
                        <button class="ui fluid button" type="submit"><?= $locale_loginButton ?></button>
                    </div>

                </form>
            </div>
        </div>

        <div class="six wide column"></div>

    <?php
        } else {
    ?>

            <div class="row"></div>

            <div class="three wide column">
                <div class="ui secondary vertical pointing menu">
                    <a class="active item">
                        Home
                    </a>
                    <a href="teachers.php" class="item">
                        Teachers
                    </a>
                    <a href="students.php" class="item">
                        Students
                    </a>
                    <a href="classes.php" class="item">
                        Classes
                    </a>
                    <a href="subjects.php" class="item">
                        Subjects
                    </a>
                </div>
            </div>

            <div class="eight wide center column">

                <?php
                if (!empty($_GET))
                {
                    if ($_GET['msg'] == 'loggedin')
                    {
                        echo('<div class="ui positive message">
                                <i class="close icon"></i>
                                <div class="header">
                                    Login successful!
                                </div>
                                <p>Hello, <strong>' . $_COOKIE['adminlogin'] . '</strong></p>
                           </div>');
                    }
                }
                ?>

                <div class="ui raised segment">

                    <div class="ui cards">

                        <?php
                            $sql = "SELECT COUNT(*) FROM teacher";
                            $sth = $dbh->prepare($sql);

                            try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                            $teachers = $sth->fetchColumn();
                        ?>
                        <div class="card">
                            <div class="content">
                                <div class="header">Teachers</div>
                                <div class="description">
                                    Teachers: <?=$teachers ?>
                                </div>
                            </div>
                            <a href="teachers.php" class="ui bottom attached button">
                                <i class="edit icon"></i>
                                Manage
                            </a>
                        </div>

                        <?php
                        $sql = "SELECT COUNT(*) FROM student";
                        $sth = $dbh->prepare($sql);

                        try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                        $students = $sth->fetchColumn();
                        ?>
                        <div class="card">
                            <div class="content">
                                <div class="header">Students</div>
                                <div class="description">
                                    Students: <?=$students ?>
                                </div>
                            </div>
                            <a href="students.php" class="ui bottom attached button">
                                <i class="edit icon"></i>
                                Manage
                            </a>
                        </div>

                        <?php
                        $sql = "SELECT COUNT(*) FROM class";
                        $sth = $dbh->prepare($sql);

                        try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                        $classes = $sth->fetchColumn();
                        ?>
                        <div class="card">
                            <div class="content">
                                <div class="header">Classes</div>
                                <div class="description">
                                    Classes: <?=$classes ?>
                                </div>
                            </div>
                            <a href="classes.php" class="ui bottom attached button">
                                <i class="edit icon"></i>
                                Manage
                            </a>
                        </div>

                        <?php
                        $sql = "SELECT COUNT(*) FROM subject";
                        $sth = $dbh->prepare($sql);

                        try { $sth->execute(); } catch (PDOException $e) { exit($e->getMessage()); }

                        $subjects = $sth->fetchColumn();
                        ?>
                        <div class="card">
                            <div class="content">
                                <div class="header">Subjects</div>
                                <div class="description">
                                    Subjects: <?=$subjects ?>
                                </div>
                            </div>
                            <a href="subjects.php" class="ui bottom attached button">
                                <i class="edit icon"></i>
                                Manage
                            </a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="three wide column"></div>

        <?php
            }
        ?>
</div>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

</body>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src='https://www.google.com/recaptcha/api.js'></script>





</html>