<?php
session_start();

require ("../includes/db.php");
require ("../includes/generateToken.php");

$token = getToken(rand(10, 20));
$_SESSION['token'] = $token;

if ($_GET['token'] == $_POST['token']) {

    // STUDENT
    if ($_GET['type'] == 'student')
    {
        if (empty($_POST['name']) ||
            empty($_POST['surname']) ||
            empty($_POST['birthday']) ||
            empty($_POST['street']) ||
            empty($_POST['building_nr']) ||
            empty($_POST['city']) ||
            empty($_POST['zipcode']) ||
            empty($_POST['pesel']) ||
            empty($_POST['email']) ||
            empty($_POST['class']))
        {
            header('Location: students.php?msg=empty');
        }
        else
        {
            // Check if new pass was given
            if (!empty($_POST['pass']))
            {
                $pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);
            }
            else
            {
                $sql = "SELECT `student`.`Password_Student`
                        FROM `student`
                        WHERE `ID_Student` = :id";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':id', $_POST['id']);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: students.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                $pass = $sth->fetchColumn();
            }

            // Parse zipcode
            $zipcode = str_replace('-','',$_POST['zipcode']);

            $sql = "UPDATE `student` 
                    SET `Name_Student` = :name,
                        `Surname_Student` = :surname,
                        `PESEL_Student` = :pesel,
                        `Password_Student` = :password,
                        `Birthday_Student` = :birthday,
                        `Email_Student` = :email,
                        `Street_Student` = :street,
                        `City_Student` = :city,
                        `Building_Student` = :building,
                        `Apartment_Student` =:apartment,
                        `Zipcode_Student` = :zipcode,
                        `Class_ID_Class` = :class
                    WHERE `student`.`ID_Student` = :id ";
            $sth = $dbh->prepare($sql);

            $sth->bindParam(':name', $_POST['name']);
            $sth->bindParam(':surname', $_POST['surname']);
            $sth->bindParam(':birthday', $_POST['birthday']);
            $sth->bindParam(':street', $_POST['street']);
            $sth->bindParam(':building', $_POST['building_nr']);
            $sth->bindParam(':apartment', $_POST['building_apt']);
            $sth->bindParam(':city', $_POST['city']);
            $sth->bindParam(':zipcode', $zipcode);
            $sth->bindParam(':pesel', $_POST['pesel']);
            $sth->bindParam(':email', $_POST['email']);
            $sth->bindParam(':class', $_POST['class']);
            $sth->bindParam(':password', $pass);
            $sth->bindParam(':id', $_POST['id']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: students.php?msg=err&err='.$e->getMessage());
                exit($e->getMessage());
            }

            header('Location: students.php?msg=success');
        }
    }

    ///////////
    // TEACHER
    ///////////

    else if ($_GET['type'] == 'teacher')
    {
        if (empty($_POST['name']) ||
            empty($_POST['surname']) ||
            empty($_POST['login']) ||
            empty($_POST['street']) ||
            empty($_POST['building_nr']) ||
            empty($_POST['city']) ||
            empty($_POST['zipcode']) ||
            empty($_POST['pesel']) ||
            empty($_POST['email']))
        {
            header('Location: teachers.php?msg=empty');
        }
        else
        {
            // Check if new pass was given
            if (!empty($_POST['pass']))
            {
                $pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);
            }
            else
            {
                $sql = "SELECT `teacher`.`Password_Teacher`
                        FROM `teacher`
                        WHERE `ID_Teacher` = :id";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':id', $_POST['id']);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: teachers.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                $pass = $sth->fetchColumn();
            }

            // Parse zipcode
            $zipcode = str_replace('-','',$_POST['zipcode']);

            $sql = "UPDATE `teacher` 
                    SET `Name_Teacher` = :name,
                        `Surname_Teacher` = :surname,
                        `PESEL_Teacher` = :pesel,
                        `Password_Teacher` = :password,
                        `Login_Teacher` = :login,
                        `Email_Teacher` = :email,
                        `Street_Teacher` = :street,
                        `City_Teacher` = :city,
                        `Building_Teacher` = :building,
                        `Apartment_Teacher` =:apartment,
                        `Zipcode_Teacher` = :zipcode
                    WHERE `teacher`.`ID_Teacher` = :id ";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':name', $_POST['name']);
            $sth->bindParam(':surname', $_POST['surname']);
            $sth->bindParam(':street', $_POST['street']);
            $sth->bindParam(':building', $_POST['building_nr']);
            $sth->bindParam(':apartment', $_POST['building_apt']);
            $sth->bindParam(':city', $_POST['city']);
            $sth->bindParam(':zipcode', $zipcode);
            $sth->bindParam(':pesel', $_POST['pesel']);
            $sth->bindParam(':login', $_POST['login']);
            $sth->bindParam(':email', $_POST['email']);
            $sth->bindParam(':password', $pass);
            $sth->bindParam(':id', $_POST['id']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: teachers.php?msg=err&err='.$e->getMessage());
                exit($e->getMessage());
            }

            header('Location: teachers.php?msg=success');
        }
    }

    ////////////
    // SUBJECT
    ////////////

    else if ($_GET['type'] == 'subject')
    {
        if (empty($_POST['name']))
        {
            header('Location: subjects.php?msg=empty');
        }
        else
        {
            $sql = "UPDATE `subject` SET `Name_Subject` = :name 
                    WHERE `subject`.`ID_Subject` = :id;";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':name', $_POST['name']);
            $sth->bindParam(':id', $_POST['id']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: subjects.php?msg=err&err=' . $e->getMessage());
                exit($e->getMessage());
            }

            header('Location: subjects.php?msg=updated');
        }
    }

    ////////////
    // CLASSES
    ////////////

    else if ($_GET['type'] == 'class')
    {
        if (empty($_POST['name']) ||
            empty($_POST['year']) ||
            empty($_POST['teacher']))
        {
            header('Location: classes.php?msg=empty');
        }
        else {
            $sql = "UPDATE `class` 
                    SET `Name_Class` = :name,
                        `Year_Class` = :year, 
                        `Teacher_ID_Teacher` = :teacher 
                    WHERE `class`.`ID_Class` = :id ";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':name', $_POST['name']);
            $sth->bindParam(':year', $_POST['year']);
            $sth->bindParam(':teacher', $_POST['teacher']);
            $sth->bindParam(':id', $_POST['id']);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: classes.php?msg=err&err='.$e->getMessage());
                exit($e->getMessage());
            }

            // Grab class-subject pairs
            $sth = $dbh->prepare('SELECT `Subject_ID_Subject` FROM `class_has_subject` WHERE `Class_ID_Class` = :id');

            $sth->bindParam(':id',$_POST['id']);

            $sth->execute();
            $classID = $sth->fetchAll();

            $classes = array();

            foreach ($classID as $i) array_push($classes, $i['Subject_ID_Subject']);

            // Check what was passed that doesn't exist in the database
            $diff = array_diff($_POST['subjects'], $classes);

            foreach ($diff as $subject) {
                $sql = "INSERT INTO class_has_subject (class_id_class, subject_id_subject)
                               VALUES (:class, :subject)";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':class', $_POST['id']);
                $sth->bindParam(':subject', $subject);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: classes.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }
            }

            // Check what exists in the database that wasn't passed
            $diff = array_diff($classes, $_POST['subjects']);

            foreach ($diff as $subject) {
                $sql = "DELETE FROM `class_has_subject`
                        WHERE `class_has_subject`.`Class_ID_Class` = :classid AND `class_has_subject`.`Subject_ID_Subject` = :subid";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':classid', $_POST['id']);
                $sth->bindParam(':subid', $subject);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: classes.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }
            }

            header('Location: edit.php?class=' . $_POST['id'] . '&classid=' . $_POST['id'] . '&token=' . $token);
        }
    }

    // ASSIGN TEACHERS TO SUBJECTS FOR A CLASS
    else if ($_GET['type'] == 'subject-teacher')
    {
        // Move only subject-teacher pairs from post to new array
        $num = count($_POST) - 2;
        $i = 0;

        $post = array();

        foreach ($_POST as $key => $val)
        {
            if(++$i > $num) break;
            $post[$key] = $val;
        }

        // Make an array of subject IDs

        $subjects = array_keys($post);
        $teachers = array_values($post);

        var_dump($subjects);
        echo '<br>';
        var_dump($teachers);
        echo '<hr>';
        var_dump($post);

        // Add teacher-subject pairs to database
        for ($j=0; $j<count($teachers); $j++)
        {
            $sql = "UPDATE `class_has_subject`
                        SET `Teacher_ID_Teacher` = :teacher
                        WHERE `class_has_subject`.`Class_ID_Class` = :class AND `class_has_subject`.`Subject_ID_Subject` = :subject";

            $sth = $dbh->prepare($sql);

            $sth->bindParam(':teacher', $teachers[$j]);
            $sth->bindParam(':class', $_POST['class']);
            $sth->bindParam(':subject', $subjects[$j]);

            try {
                $sth->execute();
            } catch (PDOException $e) {
                header('Location: classes.php?msg=err&err='.$e->getMessage());
                exit($e->getMessage());
            }

            echo count($teachers);

            if ($j==count($teachers)) break;
        }


        header('Location: classes.php?msg=assigned');
    }

    else
    {
        header('Location: index.php?msg=error');
    }

}
else
{
    echo "INVALID TOKEN<hr>GET: " . $_GET['token'] . "<br>SESSION: " . $_SESSION['token'] . "<br>POST: ". $_POST['token'];
    header('Location: index.php?msg=error');
}