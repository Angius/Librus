<?php
    require ("../includes/db.php");
    session_start();

    if ($_GET['token'] == $_POST['token'] && isset($_SESSION['adminID'])) {

        // STUDENT
        if ($_GET['type'] == 'student')
        {
            if (empty($_POST['pass']) ||
                empty($_POST['name']) ||
                empty($_POST['surname']) ||
                empty($_POST['birthday']) ||
                empty($_POST['street']) ||
                empty($_POST['building_nr']) ||
                empty($_POST['city']) ||
                empty($_POST['zipcode']) ||
                empty($_POST['pesel']) ||
                empty($_POST['email']) ||
                empty($_POST['class']))
            {
                header('Location: students.php?msg=empty');
            }
            else
            {
                $pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);
                $zipcode = str_replace('-','',$_POST['zipcode']);

                $sql = "INSERT INTO student (name_student,
                                          surname_student,
                                          birthday_student,
                                          street_student,
                                          building_student,
                                          apartment_student,
                                          city_student,
                                          zipcode_student,
                                          pesel_student,
                                          password_student,
                                          email_student,
                                          class_id_class)
                                  VALUES (:name, :surname, :birthday, :street, :building, :apartment, :city, :zipcode, :pesel, :password, :email, :class)";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':name', $_POST['name']);
                $sth->bindParam(':surname', $_POST['surname']);
                $sth->bindParam(':birthday', $_POST['birthday']);
                $sth->bindParam(':street', $_POST['street']);
                $sth->bindParam(':building', $_POST['building_nr']);
                $sth->bindParam(':apartment', $_POST['building_apt']);
                $sth->bindParam(':city', $_POST['city']);
                $sth->bindParam(':zipcode', $zipcode);
                $sth->bindParam(':pesel', $_POST['pesel']);
                $sth->bindParam(':email', $_POST['email']);
                $sth->bindParam(':class', $_POST['class']);
                $sth->bindParam(':password', $pass);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: students.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                header('Location: students.php?msg=success');
            }
        }

        // TEACHER
        else if ($_GET['type'] == 'teacher')
        {
            if (empty($_POST['pass']) ||
                empty($_POST['name']) ||
                empty($_POST['surname']) ||
                empty($_POST['login']) ||
                empty($_POST['street']) ||
                empty($_POST['building_nr']) ||
                empty($_POST['city']) ||
                empty($_POST['zipcode']) ||
                empty($_POST['pesel']) ||
                empty($_POST['email']))
            {
                header('Location: teachers.php?msg=empty');
            }
            else
            {
                $pass = password_hash($_POST['pass'], PASSWORD_BCRYPT);
                $zipcode = str_replace('-','',$_POST['zipcode']);

                $sql = "INSERT INTO teacher (name_teacher,
                                              surname_teacher,
                                              street_teacher,
                                              building_teacher,
                                              apartment_teacher,
                                              city_teacher,
                                              zipcode_teacher,
                                              pesel_teacher,
                                              login_teacher,
                                              password_teacher,
                                              email_teacher)
                                      VALUES (:name, :surname, :street, :building, :apartment, :city, :zipcode, :pesel, :login, :password, :email)";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':name', $_POST['name']);
                $sth->bindParam(':surname', $_POST['surname']);
                $sth->bindParam(':street', $_POST['street']);
                $sth->bindParam(':building', $_POST['building_nr']);
                $sth->bindParam(':apartment', $_POST['building_apt']);
                $sth->bindParam(':city', $_POST['city']);
                $sth->bindParam(':zipcode', $zipcode);
                $sth->bindParam(':pesel', $_POST['pesel']);
                $sth->bindParam(':login', $_POST['login']);
                $sth->bindParam(':email', $_POST['email']);
                $sth->bindParam(':password', $pass);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: teachers.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                header('Location: teachers.php?msg=success');
            }
        }

        // SUBJECT
        else if ($_GET['type'] == 'subject')
        {
            if (empty($_POST['name']))
            {
                header('Location: subjects.php?msg=empty');
            }
            else {
                $sql = "INSERT INTO subject (name_subject)
                                      VALUES (:name)";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':name', $_POST['name']);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: subjects.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                header('Location: subjects.php?msg=success');
            }
        }

        // CLASSES
        else if ($_GET['type'] == 'class')
        {
            if (empty($_POST['name']) ||
                empty($_POST['year']) ||
                empty($_POST['teacher']))
            {
                header('Location: classes.php?msg=empty');
            }
            else {
                $sql = "INSERT INTO class (name_class,
                                           year_class,
                                           teacher_ID_teacher)
                                      VALUES (:name, :year, :teacher)";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':name', $_POST['name']);
                $sth->bindParam(':year', $_POST['year']);
                $sth->bindParam(':teacher', $_POST['teacher']);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: classes.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                $sth = $dbh->prepare('SELECT `ID_class` FROM `class` WHERE `name_class` = "' . $_POST['name'] . '"');

                $sth->execute();
                $classID = $sth->fetchColumn();

                foreach ($_POST['subjects'] as $subject) {
                    $sql = "INSERT INTO class_has_subject (class_id_class,
                                                            subject_id_subject)
                                      VALUES (:class, :subject)";

                    $sth = $dbh->prepare($sql);

                    $sth->bindParam(':class', $classID);
                    $sth->bindParam(':subject', $subject);

                    try {
                        $sth->execute();
                    } catch (PDOException $e) {
                        header('Location: classes.php?msg=err&err='.$e->getMessage());
                        exit($e->getMessage());
                    }
                }

                header('Location: classes.php?class=' . $classID);
            }
        }

        // ASSIGN TEACHERS TO SUBJECTS FOR A CLASS
        else if ($_GET['type'] == 'subject-teacher')
        {
            // Move only subject-teacher pairs from post to new array
            $num = count($_POST) - 2;
            $i = 0;

            $post = array();

            foreach ($_POST as $key => $val)
            {
                if(++$i > $num) break;
                $post[$key] = $val;
            }

            // Make an array of subject IDs

            $subjects = array_keys($post);
            $teachers = array_values($post);

            var_dump($subjects);
            echo '<br>';
            var_dump($teachers);
            echo '<hr>';
            var_dump($post);

            // Add teacher-subject pairs to database
            for ($j=0; $j<count($teachers); $j++)
            {
                $sql = "UPDATE `class_has_subject`
                        SET `Teacher_ID_Teacher` = :teacher
                        WHERE `class_has_subject`.`Class_ID_Class` = :class AND `class_has_subject`.`Subject_ID_Subject` = :subject";

                $sth = $dbh->prepare($sql);

                $sth->bindParam(':teacher', $teachers[$j]);
                $sth->bindParam(':class', $_POST['class']);
                $sth->bindParam(':subject', $subjects[$j]);

                try {
                    $sth->execute();
                } catch (PDOException $e) {
                    header('Location: classes.php?msg=err&err='.$e->getMessage());
                    exit($e->getMessage());
                }

                echo count($teachers);

                if ($j==count($teachers)) break;
            }

            header('Location: classes.php?msg=assigned');
        }

        else
        {
            header('Location: index.php?msg=error');
        }

    }
    else
    {
        echo "INVALID TOKEN";
        header('Location: index.php?msg=error');
    }