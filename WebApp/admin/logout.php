<?php
    session_start();

    unset($_SESSION['adminID']);

    session_destroy();

    header('Location: index.php?msg=logout');