<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../semantic/dist/semantic.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        $('.message .close')
            .on('click', function() {
                $(this)
                    .closest('.message')
                    .transition('fade')
                ;
            })
        ;
    </script>

    <?php
    session_start();
    require ("../languages/en_EN.php");
    require ("../includes/db.php");
    require ("../includes/generateToken.php");

    // Check if admin
    if (!isset($_SESSION['adminID']))
    {
        header('Location: index.php');
    }
    else
    {
        // Handle token
        $token = getToken(rand(10, 20));
        $_SESSION['token'] = $token;
    };
    ?>

</head>

<body>

<div class="ui menu">
    <div class="header item">
        Librus
    </div>
    <a href="logout.php" class="right item">
        Logout
    </a>
</div>

<div class="ui middle aligned three column centered grid">

    <div class="row"></div>

    <div class="three wide column">
        <div class="ui secondary vertical pointing menu">
            <a href="index.php" class="item">
                <strong>Home</strong>
            </a>
            <a href="teachers.php" class="item">
                Teachers
            </a>
            <a href="students.php" class="item">
                Students
            </a>
            <a href="classes.php" class="item">
                Classes
            </a>
            <a href="subjects.php" class="active item">
                Subjects
            </a>
        </div>
    </div>

    <div class="eight wide center column">

        <?php // HANDLE MESSAGES
        if (!empty($_GET))
        {
            if ($_GET['msg'] == 'success')
            {
                echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                Success!
                            </div>
                            <p>Subject has been added to database!</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'updated')
            {
                echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                Success!
                            </div>
                            <p>Subject has been updated!</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'deleted')
            {
                echo('<div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                                Success!
                            </div>
                            <p>Subject has been deleted from database!</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'empty')
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                Empty fields!
                            </div>
                            <p>Name cannot be empty!</p>
                       </div>');
            }
            else if ($_GET['msg'] == 'exists')
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                This subject is being taught!
                            </div>
                            <p>You cannot delete this subject unless you delete or edit classes of the following IDs:</p>
                            <ul>');
                echo ('<li>Classes: ' . implode(', ',array_unique(explode(' ', $_GET['classes']))) . '</li>');
                echo('          </ul>
                </div>');
            }
            else if ($_GET['msg'] == 'err' && isset($_GET['err']))
            {
                echo('<div class="ui negative message">
                            <i class="close icon"></i>
                            <div class="header">
                                Database error!
                            </div>
                            <p><strong>Error code: </strong><br>' . $_GET['err'] . '</p>
                       </div>');
            }
        }
        ?>

        <?php
        // Grab subjects
        $sql = "SELECT * FROM `subject`";
        $sth = $dbh->prepare($sql);

        try {
            $sth->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }

        $subjects = $sth->fetchAll();
        ?>

        <div class="ui raised segment">

            <table class="ui selectable celled table">
                <thead>
                <tr>
                    <th class="sorted ascending">ID</th>
                    <th class="">Name</th>
                    <th class="">Teachers</th>
                    <th class="">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($subjects as $row) {

                    // Grab teachers that teach the subject
                    $sql = "SELECT DISTINCT `teacher`.`Name_Teacher`, `teacher`.`Surname_Teacher`, `subject`.`ID_Subject`
                            FROM `subject`
                                JOIN `class_has_subject` ON `class_has_subject`.`Subject_ID_Subject` = `subject`.`ID_Subject`
                                JOIN `teacher` ON `class_has_subject`.`Teacher_ID_Teacher` = `teacher`.`ID_Teacher`
                                WHERE `subject`.`ID_Subject`=:subject";

                    $sth = $dbh->prepare($sql);

                    $sth->bindParam(':subject', $row['ID_Subject']);

                    try {
                        $sth->execute();
                    } catch (PDOException $e) {
                        exit($e->getMessage());
                    }

                    $teachers = $sth->fetchAll();
                    ?>
                    <tr>
                        <td><?=$row['ID_Subject']?></td>
                        <td><?=$row['Name_Subject']?></td>
                        <td>
                            <?php
                            foreach ($teachers as $teach)
                            {
                                echo ('<div class="ui circular label">'.$teach['Surname_Teacher'].' '.$teach['Name_Teacher'].'</div>');
                            }
                            ?>
                        </td>
                        <td>
                            <div class="ui small icon buttons">
                                <a href="delete.php?subject=<?=$row['ID_Subject']?>&token=<?=$token?>" class="ui button" data-tooltip="Delete subject">
                                    <i class="trash outline icon"></i>
                                </a>
                                <a href="edit.php?subject=<?=$row['ID_Subject']?>&token=<?=$token?>" class="ui button" data-tooltip="Edit subject">
                                    <i class="edit icon"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

        </div>

    </div>

    <div class="four wide column">

        <div class="ui raised segment">

            <form class="ui form" action="add.php?type=subject&token=<?=$token?>" method="post">

                <div class="field">
                    <label>Name</label>
                    <input name="name" type="text" />
                </div>

                <input type="hidden" name="token" value="<?=$token?>" />

                <div class="field">
                    <button class="ui fluid button" type="submit" >Create</button>
                </div>
            </form>

        </div>

    </div>
</div>

<script>
    $('.message .close')
        .on('click', function() {
            $(this)
                .closest('.message')
                .transition('fade')
            ;
        })
    ;
</script>

</body>

<script src="../js/generatePassword.js"></script>
<script src="../js/showHidePassword.js"></script>

<script src="../semantic/dist/semantic.js"></script>
<script src="../js/tablesort.js"></script>

<script>$('table').tablesort()</script>

<script src='https://www.google.com/recaptcha/api.js'></script>





</html>