<?php

/**
 * Created by PhpStorm.
 * User: grzegorz
 * Date: 01.02.18
 * Time: 21:49
 */



function getStudentsGrades($studentEmail, $Name_Subject)
{
    $sql = 'SELECT distinct student_has_gradesource.Grade, gradesource.Name_GradeSource, gradesource.Date_GradeSource, librus.subject.Name_Subject, student.Name_Student, student.Surname_Student 
FROM student_has_gradesource , gradesource, librus.subject, student 
WHERE student_has_gradesource.Student_ID_Student = (select student.ID_Student WHERE student.Email_Student = :studentEmail) 
AND gradesource.ID_GradeSource = student_has_gradesource.GradeSource_ID_GradeSource AND subject.ID_Subject = gradesource.Subject_ID_Subject 
AND student.ID_Student = student_has_gradesource.Student_ID_Student
AND gradesource.Subject_ID_Subject =( SELECT ID_Subject FROM  librus.subject WHERE Name_Subject = :Name_Subject);';


    global $dbh;
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':Name_Subject' , $Name_Subject);
    $sth->bindParam(':studentEmail' , $studentEmail);

     $sth->execute();


    return $sth;



}


function getAllSubjectNames () {
    global $dbh;
    $sql = 'SELECT subject.Name_Subject FROM subject; ';
    $sth = $dbh->prepare($sql);
    $sth->execute();
    $result = $sth->fetchAll(PDO::FETCH_COLUMN, 0);
    return $result;

}


function getAllStudentsSubjects($studentID){

    global $dbh;
    $sql = 'select distinct 
		subject.Name_Subject as SUBJECT,
        teacher.Name_Teacher as TEACHER_NAME, 
		teacher.Surname_Teacher as TEACHER_SURNAME , 
		teacher.Email_Teacher as TEACHER_EMAIL 
FROM teacher, subject, class_has_subject, class, student
where
	teacher.ID_Teacher = class_has_subject.Teacher_ID_Teacher And class_has_subject.Class_ID_Class = 
    (select student.Class_ID_Class WHERE student.ID_Student = :studentID)
    AND subject.ID_Subject = class_has_subject.Subject_ID_Subject;';
    $sth = $dbh->prepare($sql);
    $sth->bindParam(':studentID' , $studentID);
    $sth->execute();

    return $sth;

}

function getAllTechers () {
    global $dbh;

    // GET TEACHERS
    $sql = "SELECT * FROM `teacher`";

    $sth = $dbh->prepare($sql);

    $sth->execute();

    $teachers = $sth->fetchAll();
    return $teachers;
}

function getAbsenseOfStudentOnSubject($studentID, $subjectName) {
    global $dbh;

    $sql = 'SELECT absence.Date_Absence as absenseDate
FROM absence_has_subject, absence, student_has_absence,
		student, subject
WHERE
	    subject.ID_Subject = (SELECT subject.ID_Subject WHERE subject.Name_Subject = :subjectName)
    AND student.ID_Student = :studentID
    AND student_has_absence.Student_ID_Student = student.ID_Student
    AND absence.ID_Absence = student_has_absence.Absence_ID_Absence
    AND absence_has_subject.absence_ID_Absence = absence.ID_Absence
    AND absence_has_subject.subject_ID_Subject = subject.ID_Subject;';

    $sth = $dbh->prepare($sql);
    $sth->bindParam(":subjectName", $subjectName);
    $sth->bindParam(":studentID", $studentID);

    $sth->execute();

    return $sth;

}


