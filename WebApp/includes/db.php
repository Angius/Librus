<?php
    define("HOST", "localhost");
    define("DBNAME", "librus");
    define("USER", "root");
    define("PASS", "");



    try {
        $dbh = new PDO('mysql:host=' . HOST . ';dbname=' . DBNAME . ';charset=utf8', USER, PASS);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo ("Error!: " . $e->getMessage() . "<br/>");
        die();
    }

    $dbh -> query ('SET NAMES utf8');

