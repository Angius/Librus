<?php
/**
 * en_EN locale file
 */

$locale_login = "Login";
$locale_password = "Password";
$locale_loginFormTitle = "Log in";
$locale_loginButton = "Log in";
$locale_loginMessage = "Logging you in...";
$locale_captcha = "Are you a robot?";
$locale_wrongcredentials_head = "Wrong credentials!";
$locale_wrongcredentials_text = "Check your login and password and try again.";
$locale_error_head = "An error occured!";
$locale_error_text = "Try again, if the error persists – contact your webmaster.";
$locale_wrongcaptcha_head = "Wrong captcha!";
$locale_wrongcaptcha_text = "You must be a robot!";
$locale_logout_head = "See you later!";
$locale_logout_text = "You've been logged out.";
$locale_loggedin_head = "Hi, "; //+ name from db
$locale_loggedin_text = "Logged in successfuly!";
$locale_home = "Home";
$locale_presence = "Presence";
$locale_grades = "Grades";
$locale_messages = "Messages";
$locale_logout = "Logout";
$locale_check = "Check presence";
$locale_grade = "Grade your students";
$locale_seemessages = "Read and send your messages";
$locale_class = "Class";
$locale_nomessages = "You have no messages.";
$locale_nogradesources = "There are no grade sources.";
$locale_successheader = "Success!";
$locale_successbody = "Item has been added to database.";
$locale_emptyheader = "Empty fields!";
$locale_emptybody = "No fields can be empty.";
$locale_errheader = "Error!";
$locale_errbody = "Contact your webmaster.<br>Error code: ";
$locale_student = "Student";
$locale_grade = "Grade";
$locale_gradebtn = "Grade";
$locale_grading = "Grading";
$locale_messagetopic = "Topic";
$locale_messagebody = "Body";
$locale_students = "Students";
$locale_send = "Send";
$locale_absence = "are absent";
$locale_subject = "Subject";
$locale_email = "Email";
$locale_teacher = "Teacher";
//$locale_ = "";
//$locale_ = "";
//$locale_ = "";
//$locale_ = "";
//$locale_ = "";
//$locale_ = "";
//$locale_ = "";